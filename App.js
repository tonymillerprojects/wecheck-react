import React from "react";
import { ApplicationProvider, Layout } from "@ui-kitten/components";
import * as eva from "@eva-design/eva";
import { Asset } from "expo-asset";
import { AppLoading, Font } from "expo";
import { Root } from "native-base";
import { StatusBar, Text, View } from "react-native";
import AppPreLoader from "./application/components/AppPreLoader";
import firebaseConfig from "./application/utils/Firebase";
import * as firebase from "firebase";
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

import LoggedNavigation from "./application/navigations/Logged";
import OfflineBar from "./application/components/OfflineBar";
import ColorsApp from "./application/utils/ColorsApp";

console.disableYellowBox = true;
console.reportErrorsAsExceptions = false;

function cacheImages(images) {
  return images.map((image) => {
    if (typeof image === "string") {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isLogged: false,
      loaded: false,
      isReady: false,
    };
  }

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require("./assets/images/header.jpg"),
      require("./assets/images/logo.png"),
      require("./assets/images/logo_dark.png"),
      require("./assets/images/emptylist.png"),
      require("./assets/images/chef.png"),
      require("./assets/images/nointernet.png"),
      require("./assets/images/contact.png"),
      require("./assets/images/servings.png"),
      require("./assets/images/calories.png"),
      require("./assets/images/checked.png"),
      require("./assets/images/cooktime.png"),
      require("./assets/images/thumb.png"),
    ]);

    await Promise.all([...imageAssets]);
  }

  async componentDidMount() {
    await Expo.Font.loadAsync({
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      "Material Icons": require("./assets/fonts//MaterialIcons.ttf"),
      MaterialIcons: require("./assets/fonts/MaterialIcons.ttf"),
      SimpleLineIcons: require("./assets/fonts/SimpleLineIcons.ttf"),
      "simple-line-icons": require("./assets/fonts/SimpleLineIcons.ttf"),
      Entypo: require("./assets/fonts/Entypo.ttf"),
      Ionicons: require("./assets/fonts/Ionicons.ttf"),
      FontAwesome: require("./assets/fonts/FontAwesome.ttf"),
    });

    await firebase.auth().onAuthStateChanged((user) => {
      console.log(user);
      if (user !== null) {
        console.log("userinfo");
        // console.log(user);
        this.setState({
          isLogged: true,
          loaded: true,
        });
      } else {
        console.log("userinfo false");
        this.setState({
          isLogged: false,
          loaded: true,
        });
      }
    });
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    const { isLogged, loaded, isReady } = this.state;
    if (!loaded) {
      return <AppPreLoader />;
    }
    return (
      <ApplicationProvider {...eva} theme={eva.light}>
        <Root>
          <OfflineBar />
          <StatusBar
            barStyle="light-content"
            translucent={true}
            backgroundColor={"transparent"}
          />
          <LoggedNavigation />
        </Root>
      </ApplicationProvider>
    );
  }
}
