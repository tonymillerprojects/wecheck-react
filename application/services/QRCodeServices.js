import * as http from '../utils/http.js';

const endpoints = {
  scanQRCode: '/scan_qr_code'
}

/**
 * { user_id: 1,
 * user_name: 'something',
 * merchant_id: 23 }
 * @param {Object} params
 */
function getQRCodeData(params) {
  return http.get(endpoints.scanQRCode, params);
}

export {
  getQRCodeData
};

