
import * as http from '../utils/http.js';

const endpoints = {
  getVenueList: '/get_venue_list',
  getRewardData: '/get_reward_data',
  claimReward: '/claim_reward'
}

/**
 * Import this file as :
 * 
 * import { getMyRewards, getMyRewardsDetails, callClaimRewardService } from '..relative path';
 * 
 */


/**
 * Send query params as object. 
 * 
 * Demo : 
 * 
 * { user_id: 1,
 * user_name: 'something',
 * merchant_id: 23 }
 * 
 * @param {Object} params
 */
function getMyRewards(params) {
  return http.get(endpoints.getVenueList, params);
}

/**
 * Send query params as object. 
 * 
 * Demo : 
 * 
 * { user_id: 1,
 * user_name: 'something',
 * merchant_id: 23 }
 * 
 * @param {Object} params
 */
function getMyRewardsDetails(params) {
  return http.get(endpoints.getRewardData, params);
}

/**
 * Send query params as object. 
 * 
 * Demo : 
 * 
 * { user_id: 1,
 * user_name: 'something',
 * merchant_id: 23 }
 * 
 * @param {Object} params
 */
function callClaimRewardService(params) {
  return http.get(endpoints.claimReward, params);
}

export {
  getMyRewards,
  getMyRewardsDetails,
  callClaimRewardService
};