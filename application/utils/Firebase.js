//////////////////// FIREBASE

// export default firebaseConfig = {
//     apiKey: "API_KEY",
//     authDomain: "AUTH_DOMAIN",
//     databaseURL: "DATABASE_URL",
//     projectId: "PROJECT_ID",
//     storageBucket: "",
//   };
import * as firebase from "firebase";
const firebaseConfig = {
  //  apiKey: "AIzaSyCX5UYGeAEo4bxs_1DFZ_V5PJVrkAifd5c",
  //     authDomain: "client-16563.firebaseapp.com",
  //     databaseURL: "https://client-16563.firebaseio.com",
  //     projectId: "client-16563",
  //     storageBucket: "client-16563.appspot.com",
  //     messagingSenderId: "407127773701",
  //     appId: "1:407127773701:web:347b1bf25728b94602c26c"
  apiKey: "AIzaSyCX5UYGeAEo4bxs_1DFZ_V5PJVrkAifd5c",
  authDomain: "client-16563.firebaseapp.com",
  databaseURL: "https://client-16563.firebaseio.com",
  projectId: "client-16563",
  storageBucket: "client-16563.appspot.com",
  messagingSenderId: "407127773701",
  appId: "1:407127773701:web:347b1bf25728b94602c26c",
};
export default firebaseConfig;
