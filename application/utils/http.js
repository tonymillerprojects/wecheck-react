import ConfigApp from './ConfigApp.js';

/**
 * Mock the fetch function.
 *
 * @param {String} url
 * @param {Object} args
 */
const http = (url, args = {}) => {
  try {
    const apiBaseUrl = ConfigApp.URL;
    const requestUrl = `${apiBaseUrl}${url}`;
    const res = await fetch(requestUrl, {
      ...args
    });

    return res.json();
  } catch (err) {
    throw err;
  }
};

/**
 * Make GET requests.
 *
 * @param {String} url
 * @param {Object} queryParams
 */
export const get = (url, queryParams) => {
  let queryString = [];
  let finalURL = url;
  Object.keys(queryParams).forEach(function(key) {
    if (queryParams[key] !== null) {
      queryString.push(`${key}=${encodeURIComponent(queryParams[key])}`);
    }
  });
  if (queryString.length) {
    finalURL = `${url}?${queryString.join('&')}`;
  }
  return http(
    finalURL,
    {
      headers: {
        accept: 'application/json;charset=UTF-8'
      }
    }
  );
};
