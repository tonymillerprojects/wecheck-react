import React, { Component } from "react";
import * as firebase from "firebase";
import { useFonts } from "@use-expo/font";
import { AppLoading } from "expo";
import {
  NavigationActions,
  StackNavigator,
  DrawerActions,
} from "react-navigation";
import * as Fonti from "expo-font";

import {
  ImageBackground,
  Dimensions,
  View,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
  Image,
  StyleSheet,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import {
  Container,
  Card,
  Header,
  Content,
  CardItem,
  Body,
  DeckSwiper,
  Text,
  Footer,
  Icon,
  Item,
  Input,
  FooterTab,
  Button,
  Left,
  Right,
  Title,
  List,
  ListItem,
  Thumbnail,
} from "native-base";

import * as Fonto from "expo-font";
import { Grid, Row, Col } from "react-native-easy-grid";
import { Ionicons, AntDesign, Entypo } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import SwiperFlatList from "react-native-swiper-flatlist";
import ConfigApp from "../utils/ConfigApp";
import RecipesHome from "../components/RecipesHome";
import GridRecipesHome from "../components/GridRecipesHome";
import CategoriesHome from "../components/CategoriesHome";
import ChefsHome from "../components/ChefsHome";
import GridView from "react-native-super-grid";
import Strings from "../utils/Strings";
import NativeBannerAd from "../components/NativeBannerAd";
import ColorsApp from "../utils/ColorsApp";

var styles = require("../../assets/files/Styles");
var { height, width } = Dimensions.get("window");
const equalWidth = width / 2;

const styleo = StyleSheet.create({
  container: {
    paddingTop: 50,
  },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  logo: {
    width: 66,
    height: 58,
  },
});

export default class Home extends Component {
  static navigationOptions = {
    defaultNavigationOptions: {
      header: null,
    },
  };

  constructor(props) {
    const cards = {
      image: require("../../assets/images/1.jpeg"),
    };

    const image = {
      uri:
        "https://i.insider.com/5db6f6a7dee01947e166b9c3?width=1100&format=jpeg&auto=webp",
      // uri1:
      //   "https://images-prod.healthline.com/hlcmsresource/images/AN_images/low-carb-fast-foods-1296x728-feature.jpg",
      // uri2:
      //   "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/kk1ojscpatwm8ptv6hue",
      // uri3:
      //   "https://www.whiskaffair.com/wp-content/uploads/2016/08/Vegetable-Hot-and-Sour-Soup-1-684x1024.jpg",
      // uri4:
      //   "https://www.crazyinspiredlife.com/wp-content/uploads/2019/08/Strawberry-Rose-Cones-Down-v2.jpg",
    };
    const imag1 = {
      uri:
        "https://images-prod.healthline.com/hlcmsresource/images/AN_images/low-carb-fast-foods-1296x728-feature.jpg",
    };
    const imag2 = {
      uri:
        "https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/kk1ojscpatwm8ptv6hue",
    };
    const imag3 = {
      uri:
        "https://www.whiskaffair.com/wp-content/uploads/2016/08/Vegetable-Hot-and-Sour-Soup-1-684x1024.jpg",
    };
    const imag4 = {
      uri:
        "https://www.crazyinspiredlife.com/wp-content/uploads/2019/08/Strawberry-Rose-Cones-Down-v2.jpg",
    };
    super(props);
    this.state = {
      string: "",
      loading: true,
      activeIndex: 0,
      fontLoaded: false,
      cardsy: [
        {
          title: "mylet",
          text: "get",
          imago: cards,
        },
      ],
      carouselItems: [
        {
          title: "ZINGER",
          text: "4.2",
          images: image,
        },
        {
          title: "SEEKH KABAB",
          text: "4.2",
          images: imag1,
        },
        {
          title: "CHINEESE",
          text: "4.2",
          images: imag2,
        },
        {
          title: "KLOOF HOUSE",
          text: "4.2",
          images: imag3,
        },
        {
          title: "ICE CREAM",
          text: "4.5",
          images: imag4,
        },
      ],
    };
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
  };

  categories() {
    const navigateAction = NavigationActions.navigate({
      routeName: "RecipesCategoriesScreen",
    });
    this.props.navigation.dispatch(navigateAction);
  }

  chefs() {
    const navigateAction = NavigationActions.navigate({
      routeName: "ChefsScreen",
    });
    this.props.navigation.dispatch(navigateAction);
  }

  search = (string) => {
    this.props.navigation.navigate("SearchScreen", {
      string: this.state.string,
    });
  };
  map = (string) => {
    this.props.navigation.navigate("Map", { string: "" });
  };

  async componentWillMount() {
    await Fonti.loadAsync({
      "Notosans-whole": require("../../assets/fonts/NotoSans-Black.ttf"),
    });
    this.setState({ fontLoaded: true });
  }
  _renderItem({ item, index }) {
    return (
      <View
        style={{
          backgroundColor: "gray",
          borderRadius: 5,
          height: 150,
          marginLeft: 10,
          marginRight: 25,
        }}
      >
        <ImageBackground
          source={item.images}
          style={{
            flex: 1,
            resizeMode: "cover",
            justifyContent: "center",
            backgroundColor: "#FFFFFF50",
          }}
        >
          <Text
            style={{
              color: "white",
              top: "20%",
              // fontFamily: "Bila",
            }}
          >
            {" "}
            <AntDesign name="star" size={24} color="white" />
            {item.text}
          </Text>
          <Text
            style={{
              fontSize: 20,
              color: "white",
              top: "22%",
              left: "3%",
              fontFamily: "Notosans-whole",
            }}
          >
            {item.title}
          </Text>
        </ImageBackground>
      </View>
    );
  }

  _renderItemmy({ item, index }) {
    return (
      <View
        style={{
          backgroundColor: "#f7f7f7",
          borderRadius: 5,
          height: 150,
          marginLeft: 10,
          marginRight: 25,
        }}
      >
        <Image
          source={item.images}
          style={{
            flex: 1,
            resizeMode: "cover",
            height: 10,
            justifyContent: "center",
            backgroundColor: "white",
          }}
        />
        <Text
          style={{
            color: "black",
            backgroundColor: "transparent",
            left: 10,
          }}
        >
          KLOOF HOUSE
          {"      "}
          {/* {item.text} */}
        </Text>
        <Text
          style={{
            fontSize: 20,
            color: "black",
            top: "22%",
            fontFamily: "Notosans-whole",
            left: "3%",
          }}
        >
          {item.title}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <Container style={styles.background_general}>
        <LinearGradient
          colors={["rgba(0,0,0,0.10)", "rgba(0,0,0,0.0)"]}
          style={{
            position: "absolute",
            top: 0,
            zIndex: 100,
            paddingTop: 45,
            paddingHorizontal: 30,
            width: width,
          }}
        ></LinearGradient>

        <ScrollView>
          <LinearGradient
            colors={["rgba(0,0,0,0.0)", "rgba(0,0,0,0.0)"]}
            style={{
              position: "absolute",
              top: 0,
              zIndex: 100,
              paddingTop: 45,
              paddingHorizontal: 30,
              width: width,
            }}
          >
            <Grid>
              <Col
                style={{
                  alignItems: "flex-start",
                  alignContent: "flex-start",
                  justifyContent: "flex-start",
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.dispatch(DrawerActions.openDrawer())
                  }
                  activeOpacity={1}
                >
                  {/* <Image
                    source={require("../../assets/images/menu.png")}
                    style={{ height: 50, width: 40 }}
                  /> */}
                  <Ionicons
                    name="md-menu"
                    style={{ fontSize: 27, color: "black" }}
                  />
                </TouchableOpacity>
              </Col>
              {this.state.fontLoaded ? (
                <Text style={{ fontFamily: "Notosans-whole" }}>Home</Text>
              ) : (
                <Text>Not Loadin</Text>
              )}
              <Col
                style={{
                  alignItems: "flex-end",
                  alignContent: "flex-end",
                  justifyContent: "flex-end",
                }}
              >
                <TouchableOpacity
                  onPress={this.search.bind(this)}
                  activeOpacity={1}
                >
                  <Ionicons
                    name="md-search"
                    style={{ fontSize: 27, color: "black" }}
                  />
                </TouchableOpacity>
              </Col>
            </Grid>
          </LinearGradient>
          <View></View>

          {/* <ImageBackground
            source={require("../../assets/images/header.jpg")}
            style={{
              flexDirection: "column",
              height: height * 0.35,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Text
              style={{
                color: "#fff",
                fontSize: 20,
                fontWeight: "bold",
                marginBottom: 5,
                marginTop: 20,
              }}
            >
              {Strings.ST38}
            </Text>
            <Text
              style={{
                color: "#fff",
                fontSize: 18,
                fontWeight: "300",
                fontFamily: "Bilal",
              }}
            >
              {Strings.ST39}
            </Text>
          </ImageBackground> */}
          {/* 
          <View
            style={{
              position: "relative",
              marginTop: -25,
              zIndex: 100,
              alignItems: "center",
              alignContent: "center",
              justifyContent: "center",
            }}
          >
            <Item rounded style={styles.itemSearch}>
              <TouchableOpacity
                onPress={this.search.bind(this, this.state.string)}
                activeOpacity={1}
              >
                <LinearGradient
                  colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                  start={[0, 0]}
                  end={[1, 0]}
                  style={styles.homesearch}
                  activeOpacity={1}
                >
                  <Ionicons
                    name="md-search"
                    style={{ fontSize: 20, color: "#FFF" }}
                  />
                </LinearGradient>
              </TouchableOpacity>
              <Input
                placeholder={Strings.ST40}
                onChangeText={(string) => this.setState({ string })}
                placeholderTextColor="#a4a4a4"
                style={{ fontSize: 15, color: "#a4a4a4" }}
              />
            </Item>
          </View> */}
          <View style={{ top: 100 }}>
            <ListItem icon style={{ borderBottomWidth: 0 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: "rgba(0,0,0,0.6)",
                    fontFamily: "Notosans-whole",
                  }}
                >
                  {/* Feture recipe */}
                  {Strings.ST10.toUpperCase()}
                </Text>
              </Body>
            </ListItem>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              {/* <Carousel
                layout={"default"}
                ref={(ref) => (this.carousel = ref)}
                data={this.state.carouselItems}
                sliderWidth={300}
                itemWidth={300}
                renderItem={this._renderItem}
                onSnapToItem={(index) => this.setState({ activeIndex: index })}
              /> */}

              <CategoriesHome />
            </View>

            <ListItem icon style={{ borderBottomWidth: 0, marginTop: 12 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text
                  style={{
                    fontSize: 14,
                    color: "rgba(0,0,0,0.6)",
                    fontFamily: "Notosans-whole",
                  }}
                >
                  {Strings.ST11.toUpperCase()}
                </Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <TouchableOpacity
                  onPress={this.categories.bind(this)}
                  activeOpacity={1}
                >
                  <View
                    style={{
                      padding: 3,
                      paddingRight: 11,
                      paddingLeft: 11,
                      borderWidth: 1,
                      borderRadius: 50,
                      borderColor: "rgba(0,0,0,0.2)",
                    }}
                  >
                    <Text style={{ fontSize: 10, color: "rgba(0,0,0,0.2)" }}>
                      {" "}
                      {Strings.ST43.toUpperCase()}
                      <Ionicons active name="ios-arrow-forward" />
                    </Text>
                  </View>
                </TouchableOpacity>
              </Right>
            </ListItem>
            {/* my owr */}
            {/* <CategoriesHome /> */}

            {/* <Card
              style={{
                elevation: 3,
              }}
            >
              <TouchableOpacity onPress={this.map.bind(this)}>
                <CardItem cardBody>
                  <Image
                    style={{ height: 200, flex: 1 }}
                    // source={require("../../assets/images/1.jpeg")}
                    source={{
                      uri:
                        "https://infatuation.s3.amazonaws.com/media/images/guides/best-outdoor-brunch-nyc/DSC00180.jpg",
                    }}
                  />
                </CardItem>

                <CardItem>
                  <Text style={{ fontSize: 13, color: "#929292" }}>
                    BREAKFAST & BRUNCH
                  </Text>
                </CardItem>
                <CardItem
                  style={{
                    top: -10,
                  }}
                >
                  <Text style={{ fontFamily: "Notosans-whole" }}>
                    KLOOF HOUSE
                  </Text>

                  <Entypo
                    name="location-pin"
                    size={24}
                    color="#929292"
                    style={{ left: 40 }}
                  />
                  <Text style={{ color: "#929292", left: 40 }}>15 KM</Text>
                  <Entypo
                    name="star"
                    size={24}
                    color="#f4d000"
                    style={{ left: 80 }}
                  />
                  <Entypo
                    name="star"
                    size={24}
                    color="#f4d000"
                    style={{ left: 80 }}
                  />
                  <Entypo
                    name="star"
                    size={24}
                    color="#f4d000"
                    style={{ left: 80 }}
                  />
                  <Entypo
                    name="star"
                    size={24}
                    color="#f4d000"
                    style={{ left: 80 }}
                  />
                </CardItem>
              </TouchableOpacity>
            </Card>
            <Card style={{ elevation: 3 }}>
              <CardItem cardBody>
                <Image
                  style={{ height: 200, flex: 1 }}
                  // source={require("../../assets/images/1.jpeg")}
                  source={{
                    uri:
                      "https://infatuation.s3.amazonaws.com/media/images/guides/best-outdoor-brunch-nyc/DSC00180.jpg",
                  }}
                />
              </CardItem>
              <CardItem>
                <Text style={{ fontSize: 13, color: "#929292" }}>
                  BREAKFAST & BRUNCH
                </Text>
              </CardItem>
              <CardItem
                style={{
                  top: -10,
                }}
              >
                <Text style={{ fontFamily: "Notosans-whole" }}>
                  KLOOF HOUSE
                </Text>

                <Entypo
                  name="location-pin"
                  size={24}
                  color="#929292"
                  style={{ left: 40 }}
                />
                <Text style={{ color: "#929292", left: 40 }}>15 KM</Text>
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
              </CardItem>
            </Card>

            <Card style={{ elevation: 3 }}>
              <CardItem cardBody>
                <Image
                  style={{ height: 200, flex: 1 }}
                  // source={require("../../assets/images/1.jpeg")}
                  source={{
                    uri:
                      "https://infatuation.s3.amazonaws.com/media/images/guides/best-outdoor-brunch-nyc/DSC00180.jpg",
                  }}
                />
              </CardItem>
              <CardItem>
                <Text style={{ fontSize: 13, color: "#929292" }}>
                  BREAKFAST & BRUNCH
                </Text>
              </CardItem>
              <CardItem
                style={{
                  top: -10,
                }}
              >
                <Text style={{ fontFamily: "Notosans-whole" }}>
                  KLOOF HOUSE
                </Text>

                <Entypo
                  name="location-pin"
                  size={24}
                  color="#929292"
                  style={{ left: 40 }}
                />
                <Text style={{ color: "#929292", left: 40 }}>15 KM</Text>
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
                <Entypo
                  name="star"
                  size={24}
                  color="#f4d000"
                  style={{ left: 80 }}
                />
              </CardItem>
            </Card> */}
            <RecipesHome />

            {/* <Carousel
              layout={"default"}
              ref={(ref) => (this.carousel = ref)}
              data={this.state.carouselItems}
              sliderWidth={300}
              itemWidth={400}
              renderItem={this._renderItemmy}
              onSnapToItem={(index) => this.setState({ activeIndex: index })}
            /> */}
            {/* <View
            style={{
              alignItems: "center",
              alignContent: "center",
              justifyContent: "center",
              marginVertical: 5,
            }}
          >
            <NativeBannerAd />
          </View> */}

            <ListItem icon style={{ borderBottomWidth: 0 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    color: "rgba(0,0,0,0.6)",
                  }}
                >
                  {Strings.ST12.toUpperCase()}
                </Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <TouchableOpacity
                  onPress={this.search.bind(this)}
                  activeOpacity={1}
                >
                  <View
                    style={{
                      padding: 3,
                      paddingRight: 11,
                      paddingLeft: 11,
                      borderWidth: 1,
                      borderRadius: 50,
                      borderColor: "rgba(0,0,0,0.2)",
                    }}
                  >
                    <Text style={{ fontSize: 10, color: "rgba(0,0,0,0.2)" }}>
                      {" "}
                      {Strings.ST43.toUpperCase()}{" "}
                      <Ionicons active name="ios-arrow-forward" />
                    </Text>
                  </View>
                </TouchableOpacity>
              </Right>
            </ListItem>

            <GridRecipesHome />

            <ListItem icon style={{ borderBottomWidth: 0 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "Notosans-whole",
                    color: "rgba(0,0,0,0.6)",
                  }}
                >
                  {Strings.ST13.toUpperCase()}
                </Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <TouchableOpacity
                  onPress={this.chefs.bind(this)}
                  activeOpacity={1}
                >
                  <View
                    style={{
                      padding: 3,
                      paddingRight: 11,
                      paddingLeft: 11,
                      borderWidth: 1,
                      borderRadius: 50,
                      borderColor: "rgba(0,0,0,0.2)",
                    }}
                  >
                    <Text style={{ fontSize: 10, color: "rgba(0,0,0,0.2)" }}>
                      {" "}
                      {Strings.ST43.toUpperCase()}{" "}
                      <Ionicons active name="ios-arrow-forward" />
                    </Text>
                  </View>
                </TouchableOpacity>
              </Right>
            </ListItem>

            <ChefsHome />

            <View style={{ height: height * 0.05 }}></View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
