import React, { Component } from "react";
import AppPreLoader from "../components/AppPreLoader";
import { NavigationActions, StackNavigator } from "react-navigation";
import {
  Dimensions,
  WebView,
  StatusBar,
  Platform,
  View,
  TouchableOpacity,
} from "react-native";
import ConfigApp from "../utils/ConfigApp";
import { ScreenOrientation } from "expo";
import { Video } from "expo-av";
import { Container, Text, Button } from "native-base";
import Icon from "react-native-vector-icons/SimpleLineIcons";

var styles = require("../../assets/files/Styles");
var { height, width } = Dimensions.get("window");

export default class VideoP extends Component {
  static navigationOptions = {
    header: null,
  };

  state = {
    isPortrait: true,
  };

  componentDidMount() {
    Dimensions.addEventListener(
      "change",
      this._orientationChangeHandler.bind(this)
    );
  }

  componentWillUnmount() {
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.PORTRAIT);
    Dimensions.removeEventListener("change", this._orientationChangeHandler);
  }

  _orientationChangeHandler(dims) {
    const { width, height } = dims.window;
    const isLandscape = width > height;
    this.setState({ isPortrait: !isLandscape });
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.ALL);
  }

  switchToLandscape() {
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.LANDSCAPE);
  }

  switchToPortrait() {
    ScreenOrientation.lockAsync(ScreenOrientation.Orientation.PORTRAIT);
  }

  render() {
    var UrlVideo = this.props.navigation.state.params.VideoLINK;

    return (
      <Container style={{ backgroundColor: "#000000" }}>
        <TouchableOpacity
          onPress={() => this.props.navigation.goBack()}
          style={{ position: "absolute", right: 15, top: 40, zIndex: 9999 }}
        >
          <View style={{ width: 30, height: 50 }}>
            <Icon name="close" style={{ color: "#fff", fontSize: 24 }} />
          </View>
        </TouchableOpacity>

        <WebView
          style={{ flex: 1, backgroundColor: "#000000" }}
          javaScriptEnabled={true}
          source={{
            uri:
              "https://www.youtube.com/embed/" +
              UrlVideo +
              "?rel=0&autoplay=0&showinfo=0&controls=0&fullscreen=1",
          }}
        />
      </Container>
    );
  }
}
