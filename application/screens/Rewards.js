import React, { Component, useState, useEffect } from "react";
import {
  NavigationActions,
  StackNavigator,
  withNavigation,
} from "react-navigation";
import { AppLoading } from "expo";
import {
  useFonts,
  NotoSans_400Regular,
  NotoSans_400Regular_Italic,
  NotoSans_700Bold,
  NotoSans_700Bold_Italic,
} from "@expo-google-fonts/noto-sans";
import {
  ImageBackground,
  Dimensions,
  View,
  ScrollView,
  ActivityIndicator,
  Alert,
  TouchableOpacity,
  FlatList,
  Image,
  StatusBar,
  StyleSheet,
  Platform,
  Modal,
} from "react-native";
import * as Font from "expo-font";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Text,
  Footer,
  Icon,
  Item,
  Input,
  FooterTab,
  Button,
  Left,
  Right,
  Title,
  List,
  ListItem,
  Thumbnail,
  Label,
} from "native-base";
import Icono from "react-native-vector-icons/Ionicons";
import axios from "axios";
import Dialog from "react-native-dialog";
import { Grid, Row, Col } from "react-native-easy-grid";
import { LinearGradient } from "expo-linear-gradient";
import { TouchableHighlight } from "react-native-gesture-handler";

// import global from './../utils/Global.js';

var { height, width } = Dimensions.get("window");
export default class SocialScreen extends React.Component {
  static navigationOptions = {
    defaultNavigationOptions: {
      title: "My Rewards",
      headerStyle: {
        backgroundColor: "#ffffff",
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        fontFamily: "Notosans-normal",
        color: "#000",
        alignItems: "center",
      },
    },
  };

  constructor(props) {
    super(props);
    this.state = {
      indexPage: null,

      title: "",
      dialogVisible: false,
      staffID: "",
      shownGroup: null,
      myrewardsList: [],
      myreward: [],
      isExtended: [false],
      current_venue_name: "",
      showModal: 1,
      alertContent: "",
    };
    // this.userid = "1234";
    this.userid = global.userID;
    console.log("reward page userid = " + this.userid);
    this.reward = {};
    this.renderItem = this.renderItem.bind(this);
  }

  componentDidMount() {
    this.getmyrewards();
    this.props.navigation.addListener("willFocus", this.load);
  }
  async componentWillMount() {
    await Fonto.loadAsync({
      "Notosans-normal": require("../../assets/fonts/NotoSans-Black.ttf"),
    });
    this.setState({ fontLoaded: true });
  }

  load = () => {
    console.log("load111");
    this.getmyrewards();
  };

  //   componentWillReceiveProps(nextProps){
  // 	// if(nextProps.someValue!==this.props.someValue){
  // 	//   //Perform some operation
  // 	//   this.setState({someState: someValue });
  // 	//   this.classMethod();
  // 	// }
  // 	this.getmyrewards();
  //   }

  getmyrewardsdetails(merchant_id) {
    // this.showLoadingView();
    let _this = this;
    var reward_url =
      "http://wecheck.co.za/rewards/index.php/api/get_reward_data?" +
      "user_id=" +
      this.userid +
      "&merchant_id=" +
      merchant_id;

    axios
      .post(reward_url)
      .then(function (response) {
        console.log(response);
        _this.setState({ myrewardsList: response.data.rewards });
        console.log("getrewards");
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  handleCancel = () => {
    this.setState({ dialogVisible: false });
    this.setState({ showModal: 1 });
  };

  handleConfirm = () => {
    // console.log("staffid="+ this.state.staffID);
    if (this.state.staffID) {
      let reward_tmp = this.state.reward;
      reward_tmp.staffcode = this.state.staffID;
      this.setState({ reward: reward_tmp });
      this.CallClaimRewardService();
    }
  };

  toggleGroup(group, index) {
    let isExtended = [false];

    if (this.isGroupShown(group)) {
      this.setState({ shownGroup: null });
      // this.setState({isExtended : false});
      isExtended[index] = false;
      this.setState({
        isExtended: isExtended,
      });
    } else {
      this.setState({ shownGroup: group });
      this.getmyrewardsdetails(group.merchant_id);
      // this.setState({isExtended : true});
      isExtended[index] = true;
      this.setState({
        isExtended: isExtended,
      });
    }
  }

  isGroupShown(group) {
    let shownGroup = this.state.shownGroup;
    return shownGroup === group;
  }

  getmyrewards() {
    let _this = this;
    console.log("getrewards call");
    var venue_list_url =
      "http://wecheck.co.za/rewards/index.php/api/get_venue_list?" +
      "user_id=" +
      this.userid;
    axios
      .post(venue_list_url)
      .then(function (response) {
        console.log("getrewards11");
        console.log("Datatatatat: ", response.data);
        console.log("getrewards11");
        _this.setState({ myreward: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  updateRewardBalance() {
    this.getmyrewards();
  }

  CallClaimRewardService() {
    let _this = this;
    // let userid = '1234';
    let userid = _this.userid;
    let url =
      "http://wecheck.co.za/rewards/index.php/api/claim_reward?" +
      "user_id=" +
      userid +
      "&merchant_id=" +
      _this.state.reward.merchant_id +
      "&reward_id=" +
      _this.state.reward.id +
      "&staff_code=" +
      _this.state.reward.staffcode;
    console.log("CallClaimRewardService url=" + url);

    axios
      .post(url)
      .then(function (response) {
        //   console.log(response);
        //   console.log('reward claim');
        if (response.data.success) {
          let mess =
            "You have claimed a " +
            _this.state.reward.title +
            " " +
            "for 5 points";
          _this.setState({ alertContent: mess });
          _this.alertModalOpen();
          _this.updateRewardBalance();
        } else {
          var message = "";
          switch (response.data.failed_reason) {
            case 1:
              message = "Invalid Staff Code";
              break;
            case 2:
              message = "Points Balance is not enough";
              break;
            default:
              message = "Please check your connection and try again!";
          }
          let mes =
            "(" +
            _this.state.reward.venue_name +
            ") Error," +
            message +
            " " +
            "error";
          _this.setState({ alertContent: message });
          _this.setState({ showModal: 3 });

          _this.showAlert(mes);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  alertModalOpen = () => {
    this.setState({ showModal: 2 });
    console.log("State Updated: ", this.state.showModal);
  };

  alertModalClose = () => {
    this.setState({ showModal: false });
  };

  showAlert(alertContent) {
    // Alert.alert(
    // 	"Hello " + this.state.showModal,
    // 	alertContent,
    // 	[
    // 	  {
    // 		text: "OK",
    // 		onPress: () => console.log("Ask me later pressed")
    // 	  },
    // 	],
    // );;
    <Modal
      visible={this.state.showModal}
      transparent={true}
      animationType="fade"
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      {console.log("I was up")}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text
            style={{
              fontSize: 16,
              fontFamily: "Notosans-normal",
              color: "green",
            }}
          >
            Congratulations
          </Text>
          <Text style={{ fontSize: 14, margin: 10 }}>{alertContent}</Text>
          <Text style={{ fontSize: 14, margin: 10 }}>Andre's Coffee House</Text>
          <View
            style={{
              flexDirection: "row",
              marginTop: 15,
              alignContent: "space-between",
            }}
          >
            <View style={{ flex: 1 }}>
              <TouchableOpacity
                style={styles.button_auth}
                onPress={this.alertModalClose}
              >
                <Text
                  style={{
                    color: "#000000",
                    fontFamily: "Notosans-normal",
                    marginTop: 15,
                  }}
                >
                  Got it
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>;
  }

  onPressRewardItem(reward, venue) {
    let reward_tmp = {};
    reward_tmp.id = reward._id;
    reward_tmp.merchant_id = reward.merchant_id;
    reward_tmp.title = reward.title;
    // reward_tmp.venue_name = venue.venue_name;
    reward_tmp.venue_name = this.state.shownGroup.venue_name;
    this.setState({ reward: reward_tmp });
    //show Dialog
    this.setState({ staffID: "" });
    this.setState({ dialogVisible: true });
  }

  renderItem(reward, index) {
    return (
      // selectedlist
      <TouchableOpacity
        style={styles.cardItemView}
        onPress={() => {
          this.onPressRewardItem(reward, index);
        }}
      >
        <View style={styles.imageView}>
          <ImageBackground
            source={{
              uri:
                "http://wecheck.co.za/rewards/index.php/api/get_reward_image/" +
                reward._id,
            }}
            style={{
              width: "100%",
              height: "90%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.onPressRewardItem(reward, index);
              }}
              style={{
                marginTop: "35%",
                height: "50%",
                width: "100%",
                backgroundColor: "rgba(0,0,0,0.5)",
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  alignContent: "center",
                  justifyContent: "center",
                  color: "white",
                  fontFamily: "Notosans-normal",
                }}
              >
                {reward.title.toUpperCase()}
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  marginVertical: 5,
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    alignContent: "center",
                    justifyContent: "center",
                    fontFamily: "Notosans-normal",
                    color: "#ffff",
                  }}
                >
                  Points required:{" "}
                </Text>
                <Text
                  style={{
                    textAlign: "center",
                    alignContent: "center",
                    justifyContent: "center",
                    fontFamily: "Notosans-normal",
                    color: "green",
                  }}
                >
                  {reward.required_points}
                </Text>
              </View>
            </TouchableOpacity>
          </ImageBackground>
        </View>
      </TouchableOpacity>
    );
  }

  renderExtendedView() {
    let selectedIndex = this.state.isExtended.indexOf(true);
    console.log("selected index = " + selectedIndex);
    return (
      selectedIndex != -1 && (
        <View style={{ flex: 1 }}>
          <View
            style={{
              justifyContent: "center",
              alignContent: "center",
              marginHorizontal: 10,
            }}
          >
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                source={require("../../assets/images/tap.png")}
                style={{ height: 20, width: 20, marginRight: 15 }}
              />
              <Text
                style={{ fontSize: 14, fontWeight: "700", color: "#787a75" }}
              >
                Tap the desired reward box to claim your reward
              </Text>
            </View>
          </View>

          <View style={{ flex: 1 }}>
            <FlatList
              style={{ width: "100%" }}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.myrewardsList}
              numColumns={2}
              renderItem={({ item, index }) => this.renderItem(item, index)}
              ItemSeparatorComponent={() => <View style={styles.separator} />}
            />
          </View>
        </View>
      )
    );
  }
  onHandleExtend() {
    let isExtended = this.state.isExtended;
    this.setState({ isExtended: !isExtended });
  }

  render() {
    return (
      <Container style={styles.background_general}>
        <LinearGradient
          colors={[
            "rgba(255,255,255,0.9)",
            "rgba(255,255,255,0.5)",
            "rgba(255,255,255,0.0)",
          ]}
          style={{
            position: "absolute",
            top: 0,
            zIndex: 100,
            paddingTop: 45,
            paddingHorizontal: 30,
            width: width,
          }}
        ></LinearGradient>
        <StatusBar barStyle="dark-content" />

        <ScrollView>
          <LinearGradient
            colors={["rgba(255,255,255,0.5)", "rgba(255,255,255,0.5)"]}
            style={{
              paddingTop: 45,
              paddingHorizontal: 30,
              width: width,
              marginBottom: 5,
            }}
          >
            <Grid>
              <Col
                style={{
                  alignItems: "flex-start",
                  alignContent: "flex-start",
                  justifyContent: "flex-start",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                  activeOpacity={1}
                >
                  <Icono
                    name="md-arrow-back"
                    style={{ fontSize: 27, color: "#000" }}
                  />
                </TouchableOpacity>
              </Col>
              <Col
                size={2}
                style={{
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    color: "#000",
                  }}
                >
                  MY REWARDS
                </Text>
              </Col>
              <Col
                style={{
                  alignItems: "flex-end",
                  alignContent: "flex-end",
                  justifyContent: "flex-end",
                }}
              >
                <TouchableOpacity
                  onPress={console.log("Share")}
                  activeOpacity={1}
                >
                  <Image
                    source={require("../../assets/images/share.png")}
                    style={{ height: 25, width: 25, marginRight: 15 }}
                  />
                </TouchableOpacity>
              </Col>
            </Grid>
          </LinearGradient>

          <View
            style={{
              justifyContent: "center",
              marginTop: Platform.OS === "android" ? 20 : 20,
              marginBottom: 10,
            }}
          >
            {this.state.myreward.map((venue, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={{
                    flexDirection: "row",
                    backgroundColor: "#fff",
                    marginBottom: 20,
                    alignItems: "center",
                    alignSelf: "center",
                    borderWidth: 1,
                    borderColor: "#ffffff",
                  }}
                  onPress={() => {
                    this.toggleGroup(venue, index);
                  }}
                >
                  <Image
                    source={{ uri: venue.mprofile_pic }}
                    style={{
                      width: 50,
                      height: 50,
                      borderRadius: 50 / 2,
                      marginRight: 10,
                    }}
                  />
                  <View style={{ flex: 0.8 }}>
                    <Text
                      style={{
                        color: "black",
                        fontFamily: "Notosans-whole",
                        color: "gray",
                      }}
                    >
                      {venue.venue_name.toUpperCase()}
                    </Text>
                    <View style={{ flexDirection: "row" }}>
                      <Text
                        style={{
                          color: "black",
                          fontFamily: "Notosans-normal",
                          fontSize: 16,
                        }}
                      >
                        Your Points Earned :{" "}
                      </Text>
                      <Text style={{ color: "green" }}>
                        {venue.points_balance}
                      </Text>
                    </View>
                  </View>
                  <Icono
                    name={
                      this.state.isExtended[index] == true
                        ? "ios-arrow-up"
                        : "ios-arrow-down"
                    }
                    style={{ fontSize: 27, color: "#000", marginRight: 20 }}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
          {this.renderExtendedView()}

          {this.state.isExtended == true && (
            <View style={{ flex: 1 }}>
              <View
                style={{
                  justifyContent: "center",
                  alignContent: "center",
                  marginHorizontal: 10,
                }}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icono
                    name="ios-hand"
                    style={{ marginHorizontal: 5, fontSize: 20, color: "#000" }}
                  />
                  <Text style={{ fontSize: 12 }}>
                    Tap the desired reward box to claim your reward
                  </Text>
                </View>
              </View>

              <View style={{ flex: 1, margin: 5 }}>
                <FlatList
                  style={{ width: "50%" }}
                  keyExtractor={(item, index) => index.toString()}
                  data={this.state.myrewardsList}
                  numColumns={2}
                  renderItem={({ item, index }) => this.renderItem(item, index)}
                  ItemSeparatorComponent={() => (
                    <View style={styles.separator} />
                  )}
                />
              </View>
            </View>
          )}
          <Modal
            visible={this.state.dialogVisible}
            transparent={true}
            animationType="fade"
          >
            {this.state.showModal == 1 && (
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={{ fontSize: 16, fontFamily: "Notosans-whole" }}>
                    Enter the staff code
                  </Text>
                  <Text style={{ fontSize: 14, margin: 10 }}>
                    Please pass your device to a staff member
                  </Text>
                  <Item rounded style={styles.inputLogin}>
                    <Input
                      style={{
                        backgroundColor: "#778899",
                        borderRadius: 50,
                        padding: 5,
                      }}
                      onChangeText={(staffID) => this.setState({ staffID })}
                      value={this.state.staffID}
                      placeholder={"Insert the Code".toUpperCase()}
                      placeholderTextColor="#778899"
                      style={{ fontSize: 16, color: "#000000" }}
                      autoCapitalize="none"
                    />
                  </Item>
                  <Text style={{ fontSize: 14, margin: 10 }}>It's ends in</Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 15,
                      alignContent: "space-between",
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={styles.button_auth}
                        onPress={this.handleCancel}
                      >
                        <Text
                          style={{
                            color: "#000000",
                            fontFamily: "Notosans-normal",
                            marginTop: 15,
                          }}
                        >
                          Back
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={styles.button_auth_2}
                        onPress={this.handleConfirm}
                      >
                        <Text
                          style={{
                            color: "#ffff",
                            fontFamily: "Notosans-normal",
                            marginTop: 15,
                          }}
                        >
                          Next
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )}
            {this.state.showModal == 2 && (
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: "Notosans-normal",
                      color: "green",
                    }}
                  >
                    Congratulations!
                  </Text>
                  <Text style={{ fontSize: 14, marginTop: 20 }}>
                    {this.state.alertContent}
                  </Text>
                  <View
                    style={{ flexDirection: "row", alignContent: "center" }}
                  >
                    <Text
                      style={{
                        color: "black",
                        fontFamily: "Notosans-normal",
                        fontSize: 14,
                        marginTop: 10,
                      }}
                    >
                      You have a total of{" "}
                    </Text>
                    <Text
                      style={{
                        color: "green",
                        fontFamily: "Notosans-normal",
                        fontSize: 14,
                        marginTop: 10,
                      }}
                    >
                      100 points
                    </Text>
                    <Text
                      style={{
                        color: "black",
                        fontFamily: "Notosans-normal",
                        fontSize: 14,
                        marginTop: 10,
                      }}
                    >
                      {" "}
                      remaining for{" "}
                    </Text>
                  </View>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: "Notosans-normal",
                      marginLeft: 10,
                      textAlign: "left",
                      marginTop: 25,
                    }}
                  >
                    Andre's Coffee House
                  </Text>
                  <View style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={styles.button_auth_3}
                        onPress={this.handleCancel}
                      >
                        <Text
                          style={{
                            color: "#ffff",
                            fontFamily: "Notosans-normal",
                            marginTop: 15,
                          }}
                        >
                          Got it
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )}

            {this.state.showModal == 3 && (
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontFamily: "Notosans-normal",
                      color: "red",
                    }}
                  >
                    invalid code!
                  </Text>
                  <Text style={{ fontSize: 14, margin: 10 }}>
                    Sorry the code you have entered is not valid
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 15,
                      alignContent: "space-between",
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={styles.button_auth}
                        onPress={this.handleCancel}
                      >
                        <Text
                          style={{
                            color: "#000000",
                            fontFamily: "Notosans-normal",
                            marginTop: 15,
                          }}
                        >
                          Try Again
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1 }}>
                      <TouchableOpacity
                        style={styles.button_auth_2}
                        onPress={this.handleCancel}
                      >
                        <Text
                          style={{
                            color: "#ffff",
                            fontFamily: "Notosans-normal",
                            marginTop: 15,
                          }}
                        >
                          Done
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            )}
          </Modal>
        </ScrollView>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    marginHorizontal: 10,
  },
  headerContainer: {
    backgroundColor: "white",
    borderBottomWidth: 0,
    width: "100%",
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  headerTitle: {
    color: "#FF0000",
  },
  modalView: {
    margin: 20,
    height: 335,
    width: 335,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  inputLogin: {
    backgroundColor: "#edf1ec",
    width: width * 0.7,
    shadowRadius: 5,
    marginBottom: 20,
    borderColor: "#edf1ec",
    margin: 15,
    left: 10,
  },

  headerIcons: {
    color: "#000000",
  },
  button: {
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10,
    width: 300,
    marginTop: 16,
  },
  button_auth: {
    width: width * 0.3,
    backgroundColor: "#ffff",
    borderWidth: 1,
    borderColor: "#000000",
    marginBottom: 5,
    height: 53,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    borderRadius: 25,
    alignItems: "center",
    alignContent: "center",
  },
  button_auth_2: {
    width: width * 0.3,
    backgroundColor: "#000000",
    borderColor: "#3f2dc0",
    marginBottom: 5,
    height: 53,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    borderRadius: 25,
    alignItems: "center",
    alignContent: "center",
  },
  button_auth_3: {
    width: width * 0.3,
    backgroundColor: "#000000",
    borderColor: "#3f2dc0",
    marginBottom: 5,
    marginTop: 25,
    height: 53,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    borderRadius: 25,
    alignItems: "center",
    alignContent: "center",
  },
  row_main: {
    width: "50%",
    paddingHorizontal: 10,
  },
  cardItemView: {
    width: "50%",
    paddingHorizontal: 10,
    marginVertical: 10,
    flex: 1,
    justifyContent: "center",
  },
  separator: {
    height: 1,
    backgroundColor: "rgba(0,0,0,0.2)",
  },
  imageView: {
    height: width / 2,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    padding: 10,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});
