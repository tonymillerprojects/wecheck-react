import React, { Component } from "react";

import {
  Alert,
  Dimensions,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  StatusBar,
} from "react-native";
import {
  Container,
  Body,
  Footer,
  Header,
  Input,
  Item,
  Left,
  Text,
  Title,
  Right,
  View,
  Button,
  Toast,
  Form,
} from "native-base";
import Icono from "react-native-vector-icons/Ionicons";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import { NavigationActions } from "react-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Modal from "react-native-modalbox";
import ConfigApp from "../utils/ConfigApp";
import Strings from "../utils/Strings";
import { LinearGradient } from "expo-linear-gradient";
import ColorsApp from "../utils/ColorsApp";
import { Grid, Row, Col } from "react-native-easy-grid";

import * as firebase from "firebase";

var styles = require("../../assets/files/Styles");
var width = Dimensions.get("window").width;

export default class Register extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor() {
    super();

    this.state = {
      name: "",
      email: "",
      surname: "",
      mobile: "",
      password: "",
      isLoading: true,
      isVisible: false,
      isOpen: false,
      isDisabled: false,
      swipeToClose: false,
      sliderValue: 0.3,
    };

    this.register = this.register.bind(this);
  }

  register() {
    const { name } = this.state;
    const { email } = this.state;
    const { password } = this.state;
    const { surname } = this.state;
    const { mobile } = this.state;

    console.log("register click");
    console.log("name=" + name);
    console.log("email=" + email);
    console.log("password=" + password);
    console.log("surname=" + surname);
    console.log("mobile=" + mobile);

    if ((name, email, password)) {
      const errorHandler = (e) => {
        console.log(e);
        if (e.code == "auth/email-already-in-use") {
          Toast.show({
            text: `${Strings.ST36}`,
            position: "bottom",
            buttonText: `${Strings.ST33}`,
          });
        } else {
          Toast.show({
            text: `${Strings.ST32}`,
            position: "bottom",
            buttonText: `${Strings.ST33}`,
          });
        }
      };

      console.log("trying to create new user.............");
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then((response) => {
          global.email = email;
          console.log("in register page result user uid=" + response.user.uid);
          global.userID = response.user.uid;

          firebase
            .auth()
            .currentUser.updateProfile({
              displayName: name,
            })
            .then(() => {
              Toast.show({
                text: `Successfully Registered`,
                position: "bottom",
                buttonText: `Ok`,
              });

              const navigateAction = NavigationActions.navigate({
                routeName: "HomeFullNavigator",
              });
              this.props.navigation.dispatch(navigateAction);
            })
            .catch(errorHandler);
        })
        .catch(errorHandler);
    }
  }

  validateEmail = (email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false) {
      this.setState({ email: email });
      return false;
    } else {
      this.setState({ email: email });
    }
  };

  validatePass = (password) => {
    let PassLength = password.length.toString();
    if (PassLength >= 6) {
      this.setState({ password: password });
      return false;
    } else {
      this.setState({ password: password });
    }
  };

  terms() {
    const navigateAction = NavigationActions.navigate({
      routeName: "TermsGuest",
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    return (
      <Container style={styles.background_general}>
        <LinearGradient
          colors={[
            "rgba(255,255,255,0.9)",
            "rgba(255,255,255,0.5)",
            "rgba(255,255,255,0.0)",
          ]}
          style={{
            position: "absolute",
            top: 0,
            zIndex: 100,
            paddingTop: 45,
            paddingHorizontal: 30,
            width: width,
          }}
        ></LinearGradient>

        <KeyboardAwareScrollView>
          <LinearGradient
            colors={["rgba(0,0,0,0.0)", "rgba(0,0,0,0.0)"]}
            style={{
              paddingTop: 45,
              paddingHorizontal: 30,
              width: width,
              marginBottom: 5,
            }}
          >
            <Grid>
              <Col
                style={{
                  alignItems: "flex-start",
                  alignContent: "flex-start",
                  justifyContent: "flex-start",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                  activeOpacity={1}
                >
                  <Icono
                    name="md-arrow-back"
                    style={{ fontSize: 27, color: "#000" }}
                  />
                </TouchableOpacity>
              </Col>
              <Col
                size={2}
                style={{
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    color: "#000",
                    fontFamily: "Notosans-whole",
                  }}
                >
                  {Strings.ST27}
                </Text>
              </Col>
              <Col
                style={{
                  alignItems: "flex-end",
                  alignContent: "flex-end",
                  justifyContent: "flex-end",
                }}
              ></Col>
            </Grid>
          </LinearGradient>

          <View
            style={{
              flex: 0.8,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              padding: 20,
            }}
          >
            <Image
              source={require("../../assets/images/logo_dark.png")}
              style={styles.logo_start}
              resizeMode="contain"
            />

            <Image
              source={require("../../assets/images/profileimage.png")}
              style={styles.profile_logo}
              resizeMode="contain"
            />

            <Form ref="formId" style={{ marginTop: 15 }}>
              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  onChangeText={(name) => this.setState({ name })}
                  placeholder={Strings.ST104}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                />
              </Item>

              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  onChangeText={(surname) => this.setState({ surname })}
                  placeholder={Strings.ST111}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                />
              </Item>

              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  onChangeText={(email) => this.validateEmail(email)}
                  value={this.state.email}
                  placeholder={Strings.ST105}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                  autoCapitalize="none"
                />
              </Item>

              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  onChangeText={(mobile) => this.setState({ mobile })}
                  placeholder={Strings.ST112}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                />
              </Item>

              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  onChangeText={(password) => this.validatePass(password)}
                  value={this.state.password}
                  placeholder={Strings.ST106}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                  secureTextEntry={true}
                  autoCapitalize="none"
                />
              </Item>
            </Form>

            <TouchableOpacity onPress={() => this.register()} activeOpacity={1}>
              <LinearGradient
                colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                start={[0, 0]}
                end={[1, 0]}
                style={styles.button_auth}
              >
                <Text
                  style={{
                    color: "#FFFFFF",
                    fontFamily: "Notosans-whole",
                    fontSize: 14,
                  }}
                >
                  {Strings.ST52.toUpperCase()}
                </Text>
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.terms.bind(this)} activeOpacity={1}>
              <Text style={styles.text_auth}>{Strings.ST51.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}
