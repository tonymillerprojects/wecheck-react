import React, { Component } from "react";
import {
  NavigationActions,
  StackNavigator,
  withNavigation,
} from "react-navigation";

import {
  Ionicons,
  FontAwesome,
  FontAwesome5,
  Entypo,
  AntDesign,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import {
  ImageBackground,
  Dimensions,
  View,
  Platform,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Linking,
  Image,
  AsyncStorage,
  StatusBar,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import Icono from "react-native-vector-icons/Ionicons";
import { Grid, Row, Col } from "react-native-easy-grid";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Tab,
  Tabs,
  ScrollableTab,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Card,
  CardItem,
  Separator,
} from "native-base";
import ConfigApp from "../utils/ConfigApp";
import Strings from "../utils/Strings";
import BannerAd from "../components/BannerAd";
import HTML from "react-native-render-html";
import * as firebase from "firebase";
import { LinearGradient } from "expo-linear-gradient";
import { Video } from "expo-av";
import ColorsApp from "../utils/ColorsApp";
import Modal from "react-native-modalbox";
import ItemForm from "../forms/ItemForm";
import ItemRating from "../components/ItemRating";
import ItemComments from "../forms/ItemComments";
import CommentList from "../components/CommentList";
import CommentEmpty from "../forms/CommentEmpty";
import Comment from "../forms/Comment";
import ToastModal from "../components/ToastModal";
import Toast from "react-native-root-toast";
import * as Fonto from "expo-font";

import PDFReader from "rn-pdf-reader-js";

import {
  Collapse,
  CollapseHeader,
  CollapseBody,
} from "accordion-collapse-react-native";

var styles = require("../../assets/files/Styles");
var { height, width } = Dimensions.get("window");

export default class RecipeDetails extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    const { params } = props.navigation.state;
    this.state = {
      item: params.item,
      mute: false,
      shouldPlay: false,
      isLoading: true,
      fontLoaded: false,
      count: 6,
      setColor: false,
    };
  }

  handlePlayAndPause = () => {
    this.setState((prevState) => ({
      shouldPlay: !prevState.shouldPlay,
    }));
  };

  saveRecipes = async (
    recipe_id,
    recipe_title,
    recipe_image,
    category_title,
    chef_title,
    recipe_time,
    recipe_description,
    recipe_ingredients,
    recipe_directions,
    recipe_notes,
    recipe_cals,
    recipe_servings,
    uid
  ) => {
    try {
      let recipe = {
        userId: uid,
        recipe_id: recipe_id,
        recipe_title: recipe_title,
        recipe_image: recipe_image,
        category_title: category_title,
        chef_title: chef_title,
        recipe_time: recipe_time,
        recipe_description: recipe_description,
        recipe_ingredients: recipe_ingredients,
        recipe_directions: recipe_directions,
        recipe_notes: recipe_notes,
        recipe_cals: recipe_cals,
        recipe_servings: recipe_servings,
      };

      const recipes = (await AsyncStorage.getItem("recipes")) || "[]";
      let recipesFav = JSON.parse(recipes);
      recipesItems = recipesFav.filter(function (e) {
        return e.recipe_id !== recipe_id && e.userId == uid;
      });
      recipesItems.push(recipe);
      AsyncStorage.setItem("recipes", JSON.stringify(recipesItems)).then(() => {
        Toast.show(Strings.ST53, {
          duration: Toast.durations.SHORT,
          position: Toast.positions.CENTER,
          shadow: false,
          animation: true,
        });
      });
    } catch (error) {
      console.log(error);
    }
  };
  componentDidMount1() {
    return fetch(ConfigApp.URL + "json/data_chefs.php")
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            isLoading: false,
            chefs: responseJson,
          },
          function () {}
        );
      })
      .catch((error) => {
        console.error(error);
      });
  }
  componentDidMount() {
    return fetch(
      ConfigApp.URL +
        "/video/index.php?item=" +
        this.props.navigation.state.params.item.recipe_id
    )
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson["hd720mp4"]) {
          let url = responseJson["hd720mp4"].url;
          this.setState(
            {
              isLoading: false,
              videoUrl: url,
            },
            function () {}
          );
        } else if (responseJson["mediummp4"]) {
          let url = responseJson["mediummp4"].url;
          this.setState(
            {
              isLoading: false,
              videoUrl: url,
            },
            function () {}
          );
        } else {
          console.log("Not Found Url");
          this.setState(
            {
              isLoading: false,
              videoUrl: "Not Found",
            },
            function () {}
          );
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  RecipesByChef = (chef_id, chef_title) => {
    this.props.navigation.navigate("RecipesByChefScreen", {
      IdChef: chef_id,
      TitleChef: chef_title,
    });
  };

  openVideo() {
    this.refs.video.open();
  }

  videoClose() {
    this.refs.video.close();
  }

  closeModal() {
    this.refs.modal3.close();
  }

  VideoPlayer = (recipe_video) => {
    this.props.navigation.navigate("VideoPlayerScreen", {
      VideoLINK: recipe_video,
    });
  };

  render() {
    const { item } = this.state;

    var user = firebase.auth().currentUser;

    return (
      <Container>
        <ScrollView>
          <LinearGradient
            colors={[
              "rgba(255,255,255,0.9)",
              "rgba(255,255,255,0.5)",
              "rgba(255,255,255,0.0)",
            ]}
            style={{
              position: "absolute",
              top: 0,
              zIndex: 100,
              paddingTop: 45,
              paddingHorizontal: 30,
              width: width,
            }}
          ></LinearGradient>
          <StatusBar barStyle="dark-content" />
          <LinearGradient
            colors={["rgba(255,255,255,0.5)", "rgba(255,255,255,0.5)"]}
            style={{
              paddingTop: 45,
              paddingHorizontal: 30,
              width: width,
              marginBottom: 5,
            }}
          >
            <Grid>
              <Col
                style={{
                  alignItems: "flex-start",
                  alignContent: "flex-start",
                  justifyContent: "flex-start",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                  activeOpacity={1}
                >
                  <Icono
                    name="md-arrow-back"
                    style={{ fontSize: 27, color: "#000" }}
                  />
                </TouchableOpacity>
              </Col>
              <Col
                size={2}
                style={{
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    color: "#000",
                    fontFamily: "Notosans-whole",
                  }}
                >
                  {item.recipe_title}
                </Text>
              </Col>
              <Col
                style={{
                  alignItems: "flex-end",
                  alignContent: "flex-end",
                  justifyContent: "flex-end",
                  top: -10,
                }}
              >
                <TouchableOpacity
                  onPress={console.log("Share")}
                  activeOpacity={1}
                >
                  <Image
                    source={require("../../assets/images/share.png")}
                    style={{ height: 25, width: 25, marginRight: 15 }}
                  />
                </TouchableOpacity>
              </Col>
            </Grid>
          </LinearGradient>

          <Tabs
            transparent
            renderTabBar={() => (
              <ScrollableTab style={{ backgroundColor: "#fff" }} />
            )}
            tabBarUnderlineStyle={{ backgroundColor: "#baaa62" }}
          >
            <Tab
              heading="Info"
              textStyle={{
                color: "#2b2b2c",
                fontWeight: "normal",
                backgroundColor: "#fff",
              }}
              activeTabStyle={{ backgroundColor: "#fff" }}
              activeTextStyle={{
                color: "#000000",
                fontWeight: "normal",
                backgroundColor: "#fff",
              }}
              tabStyle={{ backgroundColor: "#fff" }}
            >
              <View style={styles.container}>
                <View
                  style={{
                    justifyContent: "center",
                    marginTop: Platform.OS === "android" ? 20 : 20,
                    marginBottom: 10,
                  }}
                >
                  <ImageBackground
                    source={{
                      uri: `${ConfigApp.URL}images/${item.recipe_image}`,
                    }}
                    style={{
                      height: height * 0.3,
                      alignItems: "flex-start",
                      justifyContent: "flex-end",
                    }}
                    resizeMode="cover"
                  >
                    <TouchableOpacity
                      style={{
                        position: "absolute",
                        alignItems: "center",
                        justifyContent: "center",
                        zIndex: 120,
                        width: width,
                        height: height * 0.3,
                      }}
                    >
                      {/* <TouchableOpacity onPress={this.VideoPlayer.bind(this, item.recipe_video)} activeOpacity={1}> */}
                      {/* <View style={{backgroundColor: 'rgba(0,0,0,0.05)', borderRadius: 100}}> */}
                      {/* <Icon name="play-circle-outline" style={{fontSize: 55, color: '#FFFFFF'}}/> */}
                      {/* </View> */}
                      {/* </TouchableOpacity> */}
                    </TouchableOpacity>
                    <LinearGradient
                      colors={[
                        "rgba(0,0,0,0.5)",
                        "rgba(0,0,0,0.5)",
                        "rgba(0,0,0,0.5)",
                      ]}
                      style={{
                        height: height * 0.3,
                        width: width,
                        paddingHorizontal: 20,
                        alignItems: "flex-start",
                        justifyContent: "flex-end",
                        paddingBottom: 20,
                      }}
                    >
                      {/* <LinearGradient colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]} start={[0, 0]} end={[1, 0]} style={{ paddingHorizontal: 10, padding: 5, borderRadius: 5, marginBottom: 7, backgroundColor: ColorsApp.PRIMARY}}> */}
                      {/* <View style={{flexDirection: 'row'}}>
<Image source={require("../../assets/images/greentick.png")} style={{height: 20, width: 20, marginBottom: 30}}/>
<Text style={{color: '#ffff', fontSize:14,  fontWeight: "bold",alignContent: "flex-start",  marginLeft: 5 }}>Offers Rewards</Text>
</View> */}
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                          <View style={{ flexDirection: "row", marginTop: 10 }}>
                            <Image
                              source={require("../../assets/images/greentick.png")}
                              style={{ height: 20, width: 20 }}
                            />
                            <Text
                              style={{
                                color: "#ffff",
                                fontSize: 14,
                                fontFamily: "Notosans-whole",
                                alignContent: "flex-start",
                                marginLeft: 5,
                              }}
                            >
                              Offers Rewards
                            </Text>
                          </View>
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                          <View style={{ flexDirection: "row", marginTop: 10 }}>
                            <TouchableOpacity
                              onPress={this.saveRecipes.bind(
                                this,
                                item.recipe_id,
                                item.recipe_title,
                                item.recipe_image,
                                item.category_title,
                                item.chef_title,
                                item.recipe_time,
                                item.recipe_description,
                                item.recipe_ingredients,
                                item.recipe_directions,
                                item.recipe_notes,
                                item.recipe_cals,
                                item.recipe_servings,
                                item.uid
                              )}
                            >
                              <Icono
                                name="md-heart"
                                style={{ fontSize: 27, color: "#FFFFFF" }}
                              />
                            </TouchableOpacity>
                            <Text
                              style={{
                                color: "#ffff",
                                fontSize: 14,
                                fontFamily: "Notosans-whole",
                                marginLeft: 10,
                                marginTop: 5,
                              }}
                            >
                              SUBSCRIBE
                            </Text>
                          </View>
                        </View>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                          <View style={{ flexDirection: "column" }}>
                            <Text
                              style={{
                                color: "#ababab",
                                fontSize: 14,
                                fontFamily: "Notosans-whole",
                              }}
                            >
                              {item.category_title}
                            </Text>
                            <Text
                              numberOfLines={2}
                              style={{
                                fontSize: 14,
                                fontFamily: "Notosans-whole",
                                color: "#FFF",
                                marginRight: 10,
                                marginBottom: 20,
                              }}
                            >
                              {item.recipe_title}
                            </Text>
                          </View>
                        </View>
                        <View style={{ flex: 1, alignItems: "flex-end" }}>
                          <View style={{ flexDirection: "row", marginTop: 25 }}>
                            <ItemRating
                              itemId={item.recipe_id}
                              starSize={18}
                              starWidth={95}
                            />
                          </View>
                        </View>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                          <View style={{ flexDirection: "row", marginTop: 25 }}>
                            <Image
                              source={require("../../assets/images/location.png")}
                              style={{ height: 20, width: 20 }}
                            />
                            <Text
                              numberOfLines={2}
                              style={{
                                fontSize: 14,
                                fontFamily: "Notosans-whole",
                                color: "#FFF",
                                marginRight: 10,
                              }}
                            >
                              15 KM
                            </Text>
                          </View>
                        </View>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ flex: 1, alignItems: "flex-start" }}>
                          <View style={{ flexDirection: "column" }}>
                            <Text
                              numberOfLines={2}
                              style={{
                                fontSize: 14,
                                fontFamily: "Notosans-whole",
                                color: "#FFF",
                                marginRight: 10,
                                marginTop: 25,
                              }}
                            >
                              12:00 - 23: 00 - Open
                            </Text>
                          </View>
                        </View>
                      </View>
                    </LinearGradient>
                  </ImageBackground>
                  <ScrollView>
                    <View style={{ marginHorizontal: 20 }}>
                      <HTML
                        html={item.recipe_description}
                        onLinkPress={(evt, href) => {
                          Linking.openURL(href);
                        }}
                      />
                    </View>

                    <Collapse isCollapsed={true}>
                      <CollapseHeader>
                        <LinearGradient
                          colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                          start={[0, 0]}
                          end={[1, 0]}
                          style={styles.collapseStyle}
                        >
                          <Text
                            style={{
                              color: "#fff",
                              fontFamily: "Notosans-whole",
                              fontSize: 13,
                            }}
                          >
                            {Strings.ST21.toUpperCase()}
                          </Text>
                        </LinearGradient>
                      </CollapseHeader>
                      <CollapseBody>
                        <View style={{ margin: 10, backgroundColor: "#FFF" }}>
                          <HTML
                            html={item.recipe_ingredients}
                            onLinkPress={(evt, href) => {
                              Linking.openURL(href);
                            }}
                          />
                        </View>
                      </CollapseBody>
                    </Collapse>

                    <Collapse isCollapsed={true}>
                      <CollapseHeader>
                        <LinearGradient
                          colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                          start={[0, 0]}
                          end={[1, 0]}
                          style={styles.collapseStyle}
                        >
                          <Text
                            style={{
                              color: "#fff",
                              fontFamily: "Notosans-whole",
                              fontSize: 13,
                            }}
                          >
                            {Strings.ST22.toUpperCase()}
                          </Text>
                        </LinearGradient>
                      </CollapseHeader>
                      <CollapseBody>
                        <View style={{ margin: 10, backgroundColor: "#FFF" }}>
                          <HTML
                            html={item.recipe_directions}
                            onLinkPress={(evt, href) => {
                              Linking.openURL(href);
                            }}
                          />
                        </View>
                      </CollapseBody>
                    </Collapse>

                    <Collapse isCollapsed={true}>
                      <CollapseHeader>
                        <LinearGradient
                          colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                          start={[0, 0]}
                          end={[1, 0]}
                          style={styles.collapseStyle}
                        >
                          <Text
                            style={{
                              color: "#fff",
                              fontFamily: "Notosans-whole",
                              fontSize: 13,
                            }}
                          >
                            {Strings.ST14.toUpperCase()}
                          </Text>
                        </LinearGradient>
                      </CollapseHeader>
                      <CollapseBody>
                        <View
                          style={{
                            marginHorizontal: 20,
                            backgroundColor: "#FFF",
                          }}
                        >
                          <HTML
                            html={item.recipe_notes}
                            onLinkPress={(evt, href) => {
                              Linking.openURL(href);
                            }}
                          />
                        </View>
                      </CollapseBody>
                    </Collapse>

                    <View
                      style={{
                        height: 1,
                        backgroundColor: "#EEE",
                        width: width,
                      }}
                    ></View>

                    <View style={{ marginBottom: 20, backgroundColor: "#FFF" }}>
                      <ListItem icon style={{ borderBottomWidth: 0 }}>
                        <Body style={{ borderBottomWidth: 0 }}>
                          <Text
                            style={{
                              fontSize: 14,
                              fontFamily: "Notosans-whole",
                              color: "rgba(0,0,0,0.4)",
                            }}
                          >
                            {Strings.ST50.toUpperCase()}
                          </Text>
                        </Body>
                        <Right style={{ borderBottomWidth: 0 }}>
                          <TouchableOpacity
                            onPress={() => this.refs.modal3.open()}
                            activeOpacity={1}
                          >
                            <View
                              style={{
                                padding: 3,
                                paddingRight: 11,
                                paddingLeft: 11,
                                borderWidth: 1,
                                borderRadius: 50,
                                borderColor: "rgba(0,0,0,0.3)",
                              }}
                            >
                              <Text
                                style={{
                                  fontSize: 10,
                                  color: "rgba(0,0,0,0.4)",
                                }}
                              >
                                {" "}
                                {Strings.ST83.toUpperCase()}{" "}
                                <Icono active name="ios-add" />
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </Right>
                      </ListItem>

                      <View
                        style={{
                          height: 1,
                          backgroundColor: "#EEE",
                          width: width,
                          marginBottom: 5,
                        }}
                      ></View>

                      <View style={{ margin: 15, marginTop: 0 }}>
                        <ItemComments itemId={item.recipe_id} />
                      </View>
                      <View style={{ height: height * 0.1 }}></View>
                    </View>
                  </ScrollView>

                  <Modal
                    style={[styles.modal, styles.modal3]}
                    position={"center"}
                    ref="modal3"
                    swipeArea={20}
                    swipeToClose={this.state.swipeToClose}
                    onClosed={this.onClose}
                    onOpened={this.onOpen}
                    onClosingState={this.onClosingState}
                    isDisabled={this.state.isDisabled}
                    coverScreen={true}
                  >
                    <View style={{ marginTop: 8, marginBottom: 8 }}>
                      <ItemForm
                        itemId={this.state.item.recipe_id}
                        closeModal={() => this.closeModal()}
                      />
                    </View>
                  </Modal>

                  <BannerAd />
                </View>
              </View>
            </Tab>
            <Tab
              heading="Media"
              tabStyle={{ backgroundColor: "#fff" }}
              activeTabStyle={{ backgroundColor: "#fff" }}
              textStyle={{ color: "#2b2b2c", fontWeight: "normal" }}
              activeTextStyle={{ color: "#000000", fontWeight: "normal" }}
            >
              <Card>
                <CardItem cardBody>
                  <Image
                    style={{ height: 400, flex: 1, resizeMode: "cover" }}
                    // source={require("../../assets/images/1.jpeg")}
                    source={{
                      uri:
                        "https://i.pinimg.com/originals/42/80/58/4280584e0ccde2c3ccb3f3e89b94427a.jpg",
                    }}
                  />
                </CardItem>
                <CardItem>
                  <Text
                    style={{
                      fontFamily: "Notosans-whole",
                      fontFamily: "Notosans-whole",
                    }}
                  >
                    {item.chef_title.toUpperCase()}
                  </Text>
                </CardItem>
                <CardItem>
                  <Text>Cheifs Operating officer's Message for consumer</Text>
                </CardItem>
                <CardItem>
                  <Text>27 March 2020</Text>
                </CardItem>
              </Card>

              <Card>
                <CardItem cardBody>
                  <Image
                    style={{ height: 400, flex: 1, resizeMode: "cover" }}
                    // source={require("../../assets/images/1.jpeg")}
                    source={{
                      uri:
                        "https://www.gannett-cdn.com/-mm-/d7b9af8b5f0a5fe54c1803714470ee6c3b133e8a/c=0-0-2464-1392/local/-/media/2018/06/08/Bergen/NorthJersey/636640414538604242-70600kix.JPG?width=660&height=373&fit=crop&format=pjpg&auto=webp",
                    }}
                  />
                </CardItem>
                <CardItem>
                  <Text
                    style={{
                      fontFamily: "Notosans",
                      fontFamily: "Notosans-whole",
                    }}
                  >
                    {"anthony bourdain".toUpperCase()}
                  </Text>
                </CardItem>
                <CardItem>
                  <Text>Head of Sales Message for Consumers</Text>
                </CardItem>
                <CardItem>
                  <Text>27 March 2020</Text>
                </CardItem>
              </Card>
              {/* <Image
                
                style={{ height: 500, width: 500 }}
              /> */}
              <Tab />
            </Tab>
            <Tab
              heading="Menu"
              tabStyle={{ backgroundColor: "#fff" }}
              activeTabStyle={{ backgroundColor: "#fff" }}
              textStyle={{ color: "#2b2b2c", fontWeight: "normal" }}
              activeTextStyle={{ color: "#000000", fontWeight: "normal" }}
            >
              <View style={{}}>
                <PDFReader
                  style={{
                    width: Dimensions.get("window").width,
                    height: Dimensions.get("window").height,
                  }}
                  source={{
                    uri:
                      "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",
                  }}
                />
              </View>
              {/* <Text>Bilal</Text> */}
              {/* <WebView
                source={{
                  uri:
                    "https://reactnativemaster.com/wp-content/uploads/2020/02/React-native-document-viewer-pdf-sample.pdf",
                }}
              /> */}
              <Tab />
            </Tab>
            <Tab
              heading="Social"
              tabStyle={{ backgroundColor: "#fff" }}
              activeTabStyle={{ backgroundColor: "#fff" }}
              textStyle={{ color: "#2b2b2c", fontWeight: "normal" }}
              activeTextStyle={{ color: "#000000", fontWeight: "normal" }}
            >
              <Card>
                <CardItem
                  style={{
                    display: "flex",
                    justifyContent: "space-around",
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(ConfigApp.FACEBOOK);
                    }}
                  >
                    <Entypo
                      name="facebook-with-circle"
                      size={34}
                      color="#3d5285"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(ConfigApp.TWITTER);
                    }}
                  >
                    <Entypo
                      name="twitter-with-circle"
                      size={34}
                      color="#4BA2F1"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(ConfigApp.INSTAGRAM);
                    }}
                  >
                    <Entypo
                      name="instagram-with-circle"
                      size={34}
                      color="#E70076"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(ConfigApp.LINKEDIN);
                    }}
                  >
                    <Entypo
                      name="linkedin-with-circle"
                      size={34}
                      color="#007ABE"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      Linking.openURL(ConfigApp.PINTEREST);
                    }}
                  >
                    <Entypo
                      name="pinterest-with-circle"
                      size={34}
                      color="#CB0000"
                    />
                  </TouchableOpacity>
                </CardItem>
              </Card>
              <Card style={{ elevation: 3 }}>
                <CardItem style={{}}>
                  <Image
                    source={{
                      uri:
                        "https://cultivatedculture.com/wp-content/uploads/2019/12/LinkedIn-Profile-Picture-Example-Justin-Welsh.jpeg",
                    }}
                    style={{ height: 50, width: 50, borderRadius: 100 }}
                  />
                  {/* <Text style={{ fontFamily: "Notosans-normal" }}>
                    KLOOF HOUSE
                  </Text> */}

                  {/* <Entypo
                    name="location-pin"
                    size={24}
                    color="#929292"
                    style={{ left: 40 }}
                  /> */}
                  <Text>{"  "}</Text>
                  <Entypo name="google-" size={24} color="black" />
                  <Text>{"   "}</Text>
                  <Text style={{ color: "black" }}>TOMHILL</Text>
                  <MaterialCommunityIcons
                    style={{ left: 190 }}
                    name="dots-vertical"
                    size={30}
                    color="#929292"
                  />
                  {/* <FontAwesome
                    name="share"
                    size={24}
                    color="#929292"
                    
                  /> */}
                </CardItem>
                <CardItem>
                  <Text>There is nothing better than it </Text>
                  <FontAwesome5 name="burn" size={24} color="#ff7000" />
                  <Text> </Text>
                  <FontAwesome5 name="burn" size={24} color="#ff7000" />
                  <Text> </Text>
                  <FontAwesome5 name="burn" size={24} color="#ff7000" />
                </CardItem>
                <CardItem cardBody>
                  <Image
                    style={{ height: 300, flex: 1 }}
                    // source={require("../../assets/images/1.jpeg")}
                    source={{
                      uri: `${ConfigApp.URL}images/${item.recipe_image}`,
                    }}
                  />
                </CardItem>
                <CardItem>
                  <TouchableOpacity
                    onPress={() =>
                      this.setState({ count: this.state.count + 1 })
                    }
                  >
                    <MaterialCommunityIcons
                      onPress={() => this.setState({ setColor: true })}
                      name="heart-circle"
                      size={40}
                      color={!this.state.setColor ? "gray" : "red"}
                    />
                  </TouchableOpacity>
                  <Text>{!this.state.setColor ? "6" : "7"}</Text>

                  <Text>{"     "}</Text>
                  <FontAwesome
                    onPress={() =>
                      this.setState({ count: this.state.count + 1 })
                    }
                    name="comment-o"
                    size={34}
                    color="black"
                  />
                  <Text> </Text>
                  <Text>{this.state.count}</Text>
                </CardItem>

                {this.state.count > 6 ? <CommentList /> : <CommentEmpty />}

                {/* <Comment /> */}
                {/* <CardItem>
                  <Text
                    style={{
                      fontWeight: "NotoSans-Black",
                      color: "pink",
                      fontWeight: "bold",
                      left: "550%",
                      top: -10,
                    }}
                  >
                    Thomas Hill
                  </Text>
                </CardItem>
                <CardItem>
                  <Text
                    style={{
                      fontSize: 13,
                      color: "#929292",
                      left: "500%",
                      top: -20,
                    }}
                  >
                    lorem ipsum dolar sit ammit
                  </Text>
                </CardItem> */}
              </Card>
              <Tab />
            </Tab>
          </Tabs>
        </ScrollView>
      </Container>
    );
  }
}
