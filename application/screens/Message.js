import React, { useState } from "react";
import { GiftedChat, Bubble, Send } from "react-native-gifted-chat";
// var styles = require("../../assets/files/Styles");

import {
  Container,
  Content,
  Body,
  Footer,
  Header,
  Input,
  Item,
  Left,
  Text,
  Title,
  Right,
  View,
  Button,
  Toast,
  List,
  ListItem,
  Label,
  Form,
  Card,
  CardItem,
  Thumbnail,
} from "native-base";
import { StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

const styles = StyleSheet.create({
  sendingContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    backgroundColor: "white",
    display: "flex",
    justifyContent: "space-between",
    top: 10,
  },
  buttono: {
    color: "blue",
    left: 40,
  },
  buttonoDark: {
    color: "blue",
    left: 40,
  },
});
const Message = () => {
  const [dark, setDark] = useState(false);
  const [messages, setMessages] = useState([
    {
      _id: 1,
      text: "Hello, Can i help you!",
      createdAt: new Date().getTime(),
      user: {
        _id: 2,
        name: "Ahsan Shakeel",
        avatar: "https://placeimg.com/140/140/any",
      },
    },
  ]);

  const renderBubble = (props) => {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: "#487EFB",
          },
        }}
        textStyle={{
          right: {
            color: "#fff",
          },
        }}
      />
    );
  };
  const renderActions = (props) => {
    return (
      <Send {...props}>
        <View style={styles.sendingContainer}>
          {/* <IconButton icon="camera" size={32} color="#487EFB" /> */}
        </View>
      </Send>
    );
  };
  const renderSend = (props) => {
    return (
      <Send {...props}>
        <View style={styles.sendingContainer}>
          {/* <IconButton icon="send-circle" size={32} color="#487EFB" /> */}
          <Text
            style={{
              top: "-0%",
              right: "10%",
              backgroundColor: "#487EFB",
              color: "white",
              padding: "3%",
            }}
          >
            Send
          </Text>
        </View>
      </Send>
    );
  };
  const handleSend = (newMessage = []) => {
    setMessages(GiftedChat.append(messages, newMessage));
  };
  const _goBack = () => console.log("Went back");
  const _handleMore = () => console.log("Shown more");
  return (
    <Container>
      <Header style={styles.header}>
        <Body>
          <Text
            style={{
              color: "#333333",
              textAlign: "center",
              left: "0%",
              top: "30%",
              fontSize: 25,
              marginBottom: 20,
              fontFamily: "Notosans-whole",
            }}
          >
            Messages
          </Text>
        </Body>
      </Header>
      <Body
        style={{
          top: -120,
          display: "flex",
          alignItems: "center",
          flexDirection: "row",
        }}
      >
        <TouchableOpacity style={{ width: 170 }}>
          <Button disabled bordered onPress={() => setDark(true)}>
            <Text style={{ left: 40, color: "#528BFF" }}>Inbox</Text>
          </Button>
        </TouchableOpacity>
        <Button disabled bordered style={{ width: "40%" }}>
          <Text style={{ left: 40, color: "black" }}>Archive</Text>
        </Button>
      </Body>
      <Container style={{ top: -200 }}>
        <List>
          <ListItem avatar>
            <Left>
              <TouchableOpacity>
                <Thumbnail
                  source={{
                    uri:
                      "https://n7.nextpng.com/sticker-png/3/457/sticker-png-contract-computer-icons-business-document-contract-icon-company-service-logo-loan.png",
                  }}
                />
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={{ fontFamily: "Notosans-whole" }}>
                A WARM WELCOME !
              </Text>
              <Text note>
                Doing what you like will always keep you happy . .
              </Text>
            </Body>
            <Right>
              <Text note>3:43 pm</Text>
            </Right>
          </ListItem>
        </List>
        <List>
          <ListItem avatar>
            <Left>
              <TouchableOpacity>
                <Thumbnail
                  source={{
                    uri:
                      "https://n7.nextpng.com/sticker-png/3/457/sticker-png-contract-computer-icons-business-document-contract-icon-company-service-logo-loan.png",
                  }}
                />
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={{ fontFamily: "Notosans-whole" }}>
                A WARM WELCOME !
              </Text>
              <Text note>
                Doing what you like will always keep you happy . .
              </Text>
            </Body>
            <Right>
              <Text note>3:43 pm</Text>
            </Right>
          </ListItem>
        </List>
        <List>
          <ListItem avatar>
            <Left>
              <TouchableOpacity>
                <Thumbnail
                  source={{
                    uri:
                      "https://n7.nextpng.com/sticker-png/3/457/sticker-png-contract-computer-icons-business-document-contract-icon-company-service-logo-loan.png",
                  }}
                />
              </TouchableOpacity>
            </Left>
            <Body>
              <TouchableOpacity>
                <Text style={{ fontFamily: "Notosans-whole" }}>
                  How to use this app !
                </Text>
              </TouchableOpacity>
              <Text note>
                Doing what you like will always keep you happy . .
              </Text>
            </Body>
            <Right>
              <Text note>3:43 pm</Text>
            </Right>
          </ListItem>
        </List>
      </Container>
      {/* <GiftedChat
        messages={messages}
        onSend={(newMessage) => handleSend(newMessage)}
        user={{ _id: 1, name: "Bilal" }}
        renderBubble={renderBubble}
        showUserAvatar
        renderSend={renderSend}
        alwaysShowSend
        renderActions={renderActions}
      /> */}
    </Container>
  );
};
export default Message;
