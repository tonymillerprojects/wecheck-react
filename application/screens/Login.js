import React, { Component } from "react";
var styles = require("../../assets/files/Styles");
import { Alert, Dimensions, Image, TouchableOpacity } from "react-native";
import {
  Container,
  Body,
  Footer,
  Header,
  Input,
  Item,
  Left,
  Text,
  Title,
  Right,
  View,
  Button,
  Toast,
  Label,
  Form,
} from "native-base";
import * as firebase from "firebase";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import Icono from "react-native-vector-icons/Ionicons";
import { NavigationActions } from "react-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { LinearGradient } from "expo-linear-gradient";
import ColorsApp from "../utils/ColorsApp";
import { StatusBar } from "react-native";
import { Grid, Row, Col } from "react-native-easy-grid";

import Strings from "../utils/Strings";

var width = Dimensions.get("window").width;

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
    };
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", this.load);
  }

  load = () => {
    let _this = this;
    console.log("load111");
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        // User is signed in.
        const navigateAction = NavigationActions.navigate({
          routeName: "HomeFullNavigator",
        });
        _this.props.navigation.dispatch(navigateAction);
      } else {
        // No user is signed in.
      }
    });
  };

  login() {
    let { email } = this.state;
    let { password } = this.state;
    console.log("Leon login" + email + "_" + password);
    if ((email, password)) {
      console.log("Leon signInWithEmailAndPassword");
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((result) => {
          console.log("Leon login success");
          console.log(result);
          Toast.show({ text: `success`, position: "bottom", buttonText: `ok` });

          global.email = email;

          console.log("result user uid=" + result.user.uid);
          global.userID = result.user.uid;
          console.log("global userid1 = " + global.userID);

          const navigateAction = NavigationActions.navigate({
            routeName: "HomeFullNavigator",
          });
          this.props.navigation.dispatch(navigateAction);
        })
        .catch((error) => {
          const errorCode = error.code;
          console.log("Leon errorcode=" + errorCode);
          const errorMessage = error.message;
          if (errorCode === "auth/wrong-password") {
            Toast.show({
              text: `${Strings.ST30}`,
              position: "bottom",
              buttonText: `${Strings.ST33}`,
            });
          } else if (errorCode === "auth/user-not-found") {
            Toast.show({
              text: `${Strings.ST31}`,
              position: "bottom",
              buttonText: `${Strings.ST33}`,
            });
          } else {
            Toast.show({
              text: `${Strings.ST32}`,
              position: "bottom",
              buttonText: `${Strings.ST33}`,
            });
          }
        });
    }
  }

  gotoSignup() {
    const navigateAction = NavigationActions.navigate({
      routeName: "SignupScreen",
    });
    this.props.navigation.dispatch(navigateAction);
  }

  forgetpass() {
    const navigateAction = NavigationActions.navigate({
      routeName: "ForgetPass",
    });
    this.props.navigation.dispatch(navigateAction);
  }

  register() {
    const navigateAction = NavigationActions.navigate({
      routeName: "Register",
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    return (
      <Container style={styles.background_general}>
        <LinearGradient
          colors={[
            "rgba(255,255,255,0.9)",
            "rgba(255,255,255,0.5)",
            "rgba(255,255,255,0.0)",
          ]}
          style={{
            position: "absolute",
            top: 0,
            zIndex: 100,
            paddingTop: 45,
            paddingHorizontal: 30,
            width: width,
          }}
        ></LinearGradient>

        <KeyboardAwareScrollView>
          <LinearGradient
            colors={["rgba(0,0,0,0.0)", "rgba(0,0,0,0.0)"]}
            style={{
              paddingTop: 45,
              paddingHorizontal: 30,
              width: width,
              marginBottom: 5,
            }}
          >
            <Grid>
              <Col
                style={{
                  alignItems: "flex-start",
                  alignContent: "flex-start",
                  justifyContent: "flex-start",
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                  activeOpacity={1}
                >
                  <Icono
                    name="md-arrow-back"
                    style={{ fontSize: 27, color: "#000" }}
                  />
                </TouchableOpacity>
              </Col>
              <Col
                size={2}
                style={{
                  alignItems: "center",
                  alignContent: "center",
                  justifyContent: "center",
                }}
              >
                <Text
                  style={{
                    fontSize: 16,
                    color: "#000",
                    fontFamily: "Notosans-whole",
                  }}
                >
                  {Strings.ST26}
                </Text>
              </Col>
              <Col
                style={{
                  alignItems: "flex-end",
                  alignContent: "flex-end",
                  justifyContent: "flex-end",
                }}
              ></Col>
            </Grid>
          </LinearGradient>

          <View
            style={{
              flex: 0.5,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              padding: 20,
            }}
          >
            <Image
              source={require("../../assets/images/logo_dark.png")}
              style={styles.logo_start}
              resizeMode="contain"
            />

            <Form ref="formId" style={{ marginTop: 15 }}>
              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  style={styles.logo_start}
                  resizeMode="contain"
                  onChangeText={(email) => this.setState({ email })}
                  placeholder={Strings.ST109}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                  autoCapitalize="none"
                />
              </Item>

              <Item rounded={false} style={styles.inputLogin}>
                <Input
                  onChangeText={(password) => this.setState({ password })}
                  placeholder={Strings.ST106}
                  placeholderTextColor="#000000"
                  style={{ fontSize: 16, color: "#000000" }}
                  secureTextEntry={true}
                  autoCapitalize="none"
                />
              </Item>
            </Form>

            <TouchableOpacity
              style={{ marginTop: 15 }}
              onPress={this.forgetpass.bind(this)}
              style={styles.text_auth}
              activeOpacity={1}
            >
              <Text style={styles.text_auth}>{Strings.ST29.toUpperCase()}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{ marginTop: 75 }}
              onPress={this.login.bind(this)}
              activeOpacity={1}
            >
              <LinearGradient
                colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                start={[0, 0]}
                end={[1, 0]}
                style={styles.button_auth}
              >
                <Text
                  style={{
                    color: "#FFFFFF",
                    fontFamily: "Notosans-whole",
                    fontSize: 14,
                  }}
                >
                  {Strings.ST28.toUpperCase()}
                </Text>
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.gotoSignup.bind(this)}
              activeOpacity={1}
            >
              <LinearGradient
                colors={[ColorsApp.SECOND, ColorsApp.PRIMARY]}
                start={[0, 0]}
                end={[1, 0]}
                style={styles.button_auth}
              >
                <Text
                  style={{
                    color: "#FFFFFF",
                    fontFamily: "Notosans-whole",
                    fontSize: 14,
                  }}
                >
                  {"Sign Up"}
                </Text>
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.forgetpass.bind(this)}
              style={styles.text_auth}
              activeOpacity={1}
            >
              <Text style={styles.text_auth}>{Strings.ST29.toUpperCase()}</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}
