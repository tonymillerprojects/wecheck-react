import * as React from "react";
import {
  Text,
  View,
  StyleSheet,
  Button,
  Alert,
  AsyncStorage,
  Dimensions,
  Modal,
  TouchableOpacity,
} from "react-native";
import * as Permissions from "expo-permissions";
import { BarCodeScanner } from "expo-barcode-scanner";
import axios from "axios";
import moment from "moment";
// import AsyncStorage from '@react-native-community/async-storage';
var { height, width } = Dimensions.get("window");

export default class BarcodeScannerExample extends React.Component {
  state = {
    hasCameraPermission: null,
    last_scan_time: "",
    scanned: false,
    dialogVisible: false,
    qrresp: "",
    modalSwitch: true,
  };

  async componentDidMount() {
    // this.props.navigation.navigate('RewardsScreen')
    this.getPermissionsAsync();

    // AsyncStorage.getItem("last_scan_time").then((value) => {
    //   this.setState({last_scan_time: value});
    // }).done();

    AsyncStorage.getItem("last_scan_time")
      .then((value) => {
        console.log("saved time=" + value);
        let duration = moment(value).fromNow();
        let CurrentTime = moment().format();
        // let durationInHours = CurrentTime.diff(value, 'hours');

        var duration1 = moment.duration(CurrentTime.diff(value));
        var hours = duration1.asHours();

        console.log("duration=" + duration);
        console.log("durationInhour=" + durationInHours);
        // this.setState({"last_scan_time": value});
      })
      .then((res) => {
        //do something else
      });
  }

  getPermissionsAsync = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    console.log("Leon status=" + status);
    this.setState({ hasCameraPermission: status === "granted" });
  };

  saveScanTime = async (value) => {
    try {
      await AsyncStorage.setItem("last_scan_time", value);
      // getAllData();
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "flex-end",
        }}
      >
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />

        {scanned && (
          <Modal
            visible={this.state.dialogVisible}
            transparent={true}
            animationType="fade"
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text
                  style={{ fontSize: 16, fontWeight: "bold", color: "green" }}
                >
                  Congratulations!
                </Text>
                <View style={{ flexDirection: "row", alignContent: "center" }}>
                  <Text
                    style={{ color: "black", fontWeight: "bold", fontSize: 14 }}
                  >
                    You have earned a{" "}
                  </Text>
                  <Text
                    style={{ color: "green", fontWeight: "bold", fontSize: 14 }}
                  >
                    {this.state.qrresp.earned_points}
                  </Text>
                  <Text
                    style={{ color: "black", fontWeight: "bold", fontSize: 14 }}
                  >
                    {" "}
                    points from{" "}
                  </Text>
                </View>
                <Text
                  style={{ fontSize: 16, fontWeight: "bold", marginLeft: 10 }}
                >
                  {this.state.qrresp.venue_name}
                </Text>
                <View style={{ flexDirection: "row", alignContent: "center" }}>
                  <Text
                    style={{
                      color: "#d7d6d9",
                      fontWeight: "bold",
                      fontSize: 14,
                      marginTop: 10,
                    }}
                  >
                    You may scan this code again in{" "}
                    {this.state.qrresp.repeat_hours} hrs to earn more points
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 15,
                    alignContent: "space-between",
                  }}
                >
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      style={styles.button_auth}
                      onPress={this.handleCancel}
                    >
                      <Text
                        style={{
                          color: "#000000",
                          fontWeight: "bold",
                          marginTop: 15,
                        }}
                      >
                        Back
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      style={styles.button_auth_2}
                      onPress={this.handleCancel}
                    >
                      <Text
                        style={{
                          color: "#ffff",
                          fontWeight: "bold",
                          marginTop: 15,
                        }}
                      >
                        Show Rewards
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </Modal>
        )}
      </View>
    );
  }
  handleCancel = () => {
    this.setState({ dialogVisible: false });
  };
  unknownQRCode() {
    Alert.alert(
      "Unknown qr code",
      "Do you want to buy this book?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        { text: "Rescan", onPress: () => this.setState({ scanned: false }) },
      ],
      { cancelable: false }
    );
  }

  presentConfirm(resp) {
    this.setState({ qrresp: resp });

    this.setState({ dialogVisible: true });
  }

  gotoRewards() {
    this.props.navigation.navigate("RewardsScreen");
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    var keys = {};
    try {
      keys = JSON.parse(data);
    } catch {
      this.unknownQRCode();
      return;
    }
    console.log(data);
    console.log(!keys.hasOwnProperty("id"));

    if (
      !keys.hasOwnProperty("id") ||
      !keys.hasOwnProperty("name") ||
      (!keys.hasOwnProperty("email") && !keys.hasOwnProperty("venue"))
    ) {
      this.unknownQRCode();
    } else {
      let _this = this;
      let marchent_data = keys;
      let marchent_id = marchent_data.id;
      // console.log("merchant_id="+marchent_id);
      // let user_id = "1234";
      let user_id = global.userID;
      console.log("global userid = " + global.userID);
      console.log("merchant_id= " + marchent_id);

      let param =
        "user_id=" +
        user_id +
        "&" +
        "user_name=" +
        "mike pablo" +
        "&" +
        "merchant_id=" +
        marchent_id;
      let baseURL =
        "http://wecheck.co.za/rewards/index.php/api/scan_qr_code?" + param;
      axios
        .post(baseURL)
        .then(function (response) {
          console.log("venue");
          console.log(response);
          console.log("venue name=" + response.data.venue_name);
          _this.presentConfirm(response.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };
}

const styles = StyleSheet.create({
  modalView: {
    margin: 20,
    height: 335,
    width: 335,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  inputLogin: {
    backgroundColor: "#edf1ec",
    width: width * 0.8,
    shadowRadius: 5,
    marginBottom: 20,
    borderColor: "#edf1ec",
    margin: 15,
  },
  button_auth: {
    width: width * 0.3,
    backgroundColor: "#ffff",
    borderWidth: 1,
    borderColor: "#000000",
    marginBottom: 5,
    height: 53,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    borderRadius: 25,
    alignItems: "center",
    alignContent: "center",
  },
  button_auth_2: {
    width: width * 0.3,
    backgroundColor: "#000000",
    borderColor: "#3f2dc0",
    marginBottom: 5,
    height: 53,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    borderRadius: 25,
    alignItems: "center",
    alignContent: "center",
  },
  button_auth_3: {
    width: width * 0.3,
    backgroundColor: "#000000",
    borderColor: "#3f2dc0",
    marginBottom: 5,
    marginTop: 25,
    height: 53,
    shadowOpacity: 0,
    shadowRadius: 0,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    borderRadius: 25,
    alignItems: "center",
    alignContent: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    padding: 10,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
});
