import React from "react";
import SideMenu from "./SideMenu";
import Icon from "react-native-vector-icons/Ionicons";
import Icono from "react-native-vector-icons/Ionicons";
import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer,
  StackViewTransitionConfigs,
  HeaderBackButton,
} from "react-navigation";
import { Dimensions, Text, Image } from "react-native";
var styles = require("../../assets/files/Styles");

var { height, width } = Dimensions.get("window");

import HomeScreen from "../screens/Home";
import RecipesCategoriesScreen from "../screens/Categories";
import RecipesByCategoryScreen from "../screens/RecipesByCategory";
import RecipesByChefScreen from "../screens/RecipesByChef";
import RecipeDetailsScreen from "../screens/RecipeDetails";
import ChefsScreen from "../screens/Chefs";
import SearchScreen from "../screens/Search";
import FavoritesScreen from "../screens/Favorites";
import LogoutScreen from "../screens/Logout";
import SettingsScreen from "../screens/Settings";
import TermsScreen from "../screens/Terms";
import AboutUsScreen from "../screens/AboutUs";
import RewardsScreen from "../screens/Rewards";
import ContactUsScreen from "../screens/ContactUs";
import ScanScreen from "../screens/QRCodeScanner";
import VideoPlayerScreen from "../screens/VideoPlayer";
import LoginScreen from "../screens/Login";
import SignupScreen from "../screens/Register";
import MapScreen from "../screens/Map";
import MessageScreen from "../screens/Message";
import { createBottomTabNavigator } from "react-navigation-tabs";

const leftIcon = (navigation, icon) => (
  <Icon
    name={icon}
    style={{ marginLeft: 20 }}
    size={27}
    color="white"
    onPress={() => navigation.navigate("DrawerOpen")}
  />
);

const navigationOptions = {
  defaultNavigationOptions: {
    header: null,
  },
};

const HomeNavigator = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,
      // navigationOptions: ({navigation}) => ({
      // headerLeft: leftIcon(navigation, 'md-menu'),
      // title: "Home",
    },
    RecipesCategoriesScreen: {
      screen: RecipesCategoriesScreen,
    },
    RecipesByCategoryScreen: {
      screen: RecipesByCategoryScreen,
    },
    RecipesByChefScreen: {
      screen: RecipesByChefScreen,
    },
    RecipeDetailsScreen: {
      screen: RecipeDetailsScreen,
    },
    ChefsScreen: {
      screen: ChefsScreen,
    },
    SearchScreen: {
      screen: SearchScreen,
    },
    FavoritesScreen: {
      screen: FavoritesScreen,
    },
    LogoutScreen: {
      screen: LogoutScreen,
    },
    SettingsScreen: {
      screen: SettingsScreen,
    },
    AboutUsScreen: {
      screen: AboutUsScreen,
    },
    RewardsScreen: {
      screen: RewardsScreen,
    },
    TermsScreen: {
      screen: TermsScreen,
    },
    ContactUsScreen: {
      screen: ContactUsScreen,
    },
    VideoPlayerScreen: {
      screen: VideoPlayerScreen,
    },
    MapScreen: {
      screen: MapScreen,
    },
    MessageScreen: {
      screen: MessageScreen,
    },
  },
  navigationOptions
);

const HomeWithDrawer = createDrawerNavigator(
  {
    Home: {
      screen: HomeNavigator,
    },
  },
  {
    contentComponent: SideMenu,
    drawerWidth: width * 0.7,
    drawerOpenRoute: "DrawerOpen",
    drawerCloseRoute: "DrawerClose",
    drawerToggleRoute: "DrawerToggle",
  }
);

const HomeFullNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeWithDrawer,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require("../../assets/images/home.png")}
          style={{ height: 25, width: 25 }}
        />
      ),
    },
  },

  QR: {
    screen: ScanScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require("../../assets/images/qr-code.png")}
          style={{ height: 25, width: 25 }}
        />
      ),
    },
  },
  Search: {
    screen: SearchScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require("../../assets/images/search.png")}
          style={{ height: 25, width: 25 }}
        />
      ),
    },
  },
  Message: {
    screen: MessageScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require("../../assets/images/messages.png")}
          style={{ height: 25, width: 25 }}
        />
      ),
    },
  },
  Map: {
    screen: MapScreen,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require("../../assets/images/map.png")}
          style={{ height: 25, width: 25 }}
        />
      ),
    },
  },
});

//
const HomeStack = createStackNavigator(
  {
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => ({
        headerLeft: leftIcon(navigation, "md-menu"),
      }),
    },

    SignupScreen: {
      screen: SignupScreen,
    },

    HomeFullNavigator: {
      screen: HomeFullNavigator,
    },
  },
  navigationOptions
);
//

export default createAppContainer(HomeFullNavigator);
// export default createAppContainer(HomeStack);
