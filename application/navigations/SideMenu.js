// import PropTypes from "prop-types";
// import React, { Component } from "react";
// import { NavigationActions } from "react-navigation";
// import { DrawerItem } from "@ui-kitten/components";
// import {
//   Dimensions,
//   ScrollView,
//   View,
//   Image,
//   TouchableOpacity,
// } from "react-native";
// import {
//   Container,
//   Header,
//   Content,
//   List,
//   ListItem,
//   Text,
//   Left,
//   Thumbnail,
//   Button,
//   Body,
//   Right,
//   Switch,
// } from "native-base";
// import Strings from "../utils/Strings";
// import Icon from "react-native-vector-icons/SimpleLineIcons";

// var { height, width } = Dimensions.get("window");
// var styles = require("../../assets/files/Styles");

// class SideMenu extends Component {
//   constructor() {
//     const ForwardIcon = (props) => (
//       <Icon {...props} name="arrow-right" style={styles.icon_menu} />
//     );
//     const TagIcon = (props) => (
//       <Icon {...props} name="tag" style={styles.iconSidemenu} />
//     );
//     const UserIcon = (props) => (
//       <Icon {...props} name="user" style={styles.iconSidemenu} />
//     );
//     const EnvelopIcon = (props) => (
//       <Icon {...props} name="envelope-letter" style={styles.iconSidemenu} />
//     );
//     const MagnifierIcon = (props) => (
//       <Icon {...props} name="magnifier" style={styles.iconSidemenu} />
//     );
//     const HeartIcon = (props) => (
//       <Icon {...props} name="heart" style={styles.iconSidemenu} />
//     );
//     const SettingIcon = (props) => (
//       <Icon {...props} name="settings" style={styles.iconSidemenu} />
//     );
//     const LoginIcon = (props) => (
//       <Icon {...props} name="login" style={styles.iconSidemenu} />
//     );

//     super();
//     this.state = {
//       icon: ForwardIcon,
//       TagIcon: TagIcon,
//       UserIcon: UserIcon,
//       EnvelopIcon: EnvelopIcon,
//       MagnifierIcon: MagnifierIcon,
//       HeartIcon: HeartIcon,
//       SettingIcon: SettingIcon,
//       LoginIcon: LoginIcon,
//     };
//   }
//   navigateToScreen = (route) => () => {
//     const navigateAction = NavigationActions.navigate({
//       routeName: route,
//     });
//     this.props.navigation.dispatch(navigateAction);
//   };

//   search = (string) => {
//     this.props.navigation.navigate("SearchScreen", { string: "" });
//   };

//   rewards = (string) => {
//     this.props.navigation.navigate("RewardsScreen", { string: "" });
//   };
//   Login = (string) => {
//     this.props.navigation.navigate("Login", { string: "" });
//   };

//   render() {
//     return (
//       <View style={styles.container_menu}>
//         <View style={styles.sideMenu}>
//           <Image
//             source={require("../../assets/images/logo.png")}
//             style={{ flex: 1, width: 220, height: 220 }}
//             resizeMode="contain"
//           />
//         </View>

//         <ScrollView>
//           <ListItem style={styles.item_menu} icon>
//             <DrawerItem
//               onPress={this.navigateToScreen("RecipesCategoriesScreen")}
//               title={Strings.ST2.toUpperCase()}
//               accessoryLeft={this.state.TagIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="tag" style={styles.iconSidemenu} />
//             </Left>
//             <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{Strings.ST2.toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>

//           <ListItem style={styles.item_menu} icon>
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="user" style={styles.iconSidemenu} />
//             </Left> */}
//             <DrawerItem
//               onPress={this.navigateToScreen("ChefsScreen")}
//               title={Strings.ST3.toUpperCase()}
//               accessoryLeft={this.state.UserIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{Strings.ST3.toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>

//           <ListItem style={styles.item_menu} icon>
//             <DrawerItem
//               onPress={this.navigateToScreen("RewardsScreen")}
//               title={"My Rewards".toUpperCase()}
//               accessoryLeft={this.state.EnvelopIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="envelope-letter" style={styles.iconSidemenu} />
//             </Left>
//             <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{"My Rewards".toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>

//           <ListItem style={styles.item_menu} icon>
//             <DrawerItem
//               onPress={this.search.bind(this)}
//               title={Strings.ST4.toUpperCase()}
//               accessoryLeft={this.state.MagnifierIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="magnifier" style={styles.iconSidemenu} />
//             </Left>
//             <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{Strings.ST4.toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>

//           <ListItem style={styles.item_menu} icon>
//             <DrawerItem
//               onPress={this.navigateToScreen("FavoritesScreen")}
//               title={Strings.ST6.toUpperCase()}
//               accessoryLeft={this.state.HeartIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="heart" style={styles.iconSidemenu} />
//             </Left>
//             <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{Strings.ST6.toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>

//           <ListItem
//             style={styles.item_menu}
//             // onPress={this.navigateToScreen("SettingsScreen")}
//             icon
//           >
//             <DrawerItem
//               onPress={this.navigateToScreen("SettingsScreen")}
//               title={Strings.ST7.toUpperCase()}
//               accessoryLeft={this.state.SettingIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="settings" style={styles.iconSidemenu} />
//             </Left>
//             <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{Strings.ST7.toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>

//           <ListItem style={styles.item_menu} icon>
//             <DrawerItem
//               onPress={this.navigateToScreen("Login")}
//               title={Strings.ST8.toUpperCase()}
//               accessoryLeft={this.state.LoginIcon}
//               accessoryRight={this.state.icon}
//             />
//             {/* <Left style={{ borderBottomWidth: 0 }}>
//               <Icon name="login" style={styles.iconSidemenu} />
//             </Left>
//             <Body style={{ borderBottomWidth: 0 }}>
//               <Text style={styles.text_menu}>{Strings.ST8.toUpperCase()}</Text>
//             </Body>
//             <Right style={{ borderBottomWidth: 0 }}>
//               <Icon name="arrow-right" style={styles.icon_menu} />
//             </Right> */}
//           </ListItem>
//         </ScrollView>
//       </View>
//     );
//   }
// }

// SideMenu.propTypes = {
//   navigation: PropTypes.object,
// };
import React from "react";
import PropTypes from "prop-types";
import {
  Divider,
  Drawer,
  DrawerItem,
  IndexPath,
  useTheme,
  IconRegistry,
} from "@ui-kitten/components";
import { NavigationActions } from "react-navigation";
import Strings from "../utils/Strings";
import Icon from "react-native-vector-icons/SimpleLineIcons";
import {
  Dimensions,
  ScrollView,
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from "react-native";
// import Icon from "react-native-vector-icons/Simple
var { height, width } = Dimensions.get("window");
var styles = require("../../assets/files/Styles");
const SideMenu = (props) => {
  const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
  const ForwardIcon = (props) => (
    <Icon {...props} name="arrow-right" style={styles.icon_menu} />
  );
  const TagIcon = (props) => (
    <Icon {...props} name="tag" style={styles.iconSidemenu} />
  );
  const UserIcon = (props) => (
    <Icon {...props} name="user" style={styles.iconSidemenu} />
  );
  const EnvelopIcon = (props) => (
    <Icon {...props} name="envelope-letter" style={styles.iconSidemenu} />
  );
  const MagnifierIcon = (props) => (
    <Icon {...props} name="magnifier" style={styles.iconSidemenu} />
  );
  const HeartIcon = (props) => (
    <Icon {...props} name="heart" style={styles.iconSidemenu} />
  );
  const SettingIcon = (props) => (
    <Icon {...props} name="settings" style={styles.iconSidemenu} />
  );
  const LoginIcon = (props) => (
    <Icon {...props} name="login" style={styles.iconSidemenu} />
  );

  const Header = (props) => (
    <React.Fragment>
      <View style={styles.sideMenu}>
        <Image
          source={require("../../assets/images/logo.png")}
          style={{ flex: 1, width: 220, height: 220 }}
          resizeMode="contain"
        />
      </View>
      <Divider />
    </React.Fragment>
  );
  const search = (string, props) => {
    navigation.navigate("SearchScreen", { string: "" });
  };

  const rewards = (string) => {
    navigation.navigate("RewardsScreen", { string: "" });
  };
  const Login = (string) => {
    navigation.navigate("Login", { string: "" });
  };
  const navigateToScreen = (route) => (props) => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    navigation.dispatch(navigateAction);
  };

  return (
    <Drawer
      header={Header}
      selectedIndex={selectedIndex}
      onSelect={(index) => setSelectedIndex(index)}
    >
      <DrawerItem
        onPress={() => props.navigation.navigate("RecipesCategoriesScreen")}
        title={Strings.ST2.toUpperCase()}
        accessoryLeft={TagIcon}
        accessoryRight={ForwardIcon}
      />
      <DrawerItem
        onPress={() => props.navigation.navigate("ChefsScreen")}
        title={Strings.ST3.toUpperCase()}
        accessoryLeft={UserIcon}
        accessoryRight={ForwardIcon}
      />

      <DrawerItem
        onPress={() => props.navigation.navigate("RewardsScreen")}
        title={"My Rewards".toUpperCase()}
        accessoryLeft={EnvelopIcon}
        accessoryRight={ForwardIcon}
      />
      <DrawerItem
        onPress={() => props.navigation.navigate("SearchScreen")}
        title={Strings.ST4.toUpperCase()}
        accessoryLeft={MagnifierIcon}
        accessoryRight={ForwardIcon}
      />
      <DrawerItem
        onPress={() => props.navigation.navigate("FavoritesScreen")}
        title={Strings.ST6.toUpperCase()}
        accessoryLeft={HeartIcon}
        accessoryRight={ForwardIcon}
      />
      <DrawerItem
        onPress={() => props.navigation.navigate("SettingsScreen")}
        title={Strings.ST7.toUpperCase()}
        accessoryLeft={SettingIcon}
        accessoryRight={ForwardIcon}
      />
      <DrawerItem
        onPress={() => props.navigation.navigate("Login")}
        title={Strings.ST8.toUpperCase()}
        accessoryLeft={LoginIcon}
        accessoryRight={ForwardIcon}
      />
    </Drawer>
  );
};
// const styles = StyleSheet.create({
//   header: {
//     height: 128,
//     flexDirection: "row",
//     alignItems: "center",
//   },
// });

export default SideMenu;
