import React, { PureComponent } from "react";
import {
  NavigationActions,
  StackNavigator,
  withNavigation,
} from "react-navigation";
import {
  ImageBackground,
  Dimensions,
  View,
  ActivityIndicator,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { Container, Text } from "native-base";
import Icono from "react-native-vector-icons/Ionicons";
import Carousel from "react-native-snap-carousel";
import ConfigApp from "../utils/ConfigApp";
import AppPreLoader from "./AppPreLoader";
import Strings from "../utils/Strings";
import ColorsApp from "../utils/ColorsApp";
import ItemRating from "./ItemRating";

var { height, width } = Dimensions.get("window");

class CategoriesHome extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
    this._carousel = {};
  }

  componentDidMount() {
    console.log("Compon");

    return fetch("http://wecheck.co.za/admin/json/data_categories.php")
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("Response: ", responseJson);
        this.setState(
          {
            isLoading: false,
            categories: responseJson,
          },
          function () {}
        );
      })
      .catch((error) => {
        console.error(error);
      });
  }

  RecipesByCategory = (category_id, category_title) => {
    this.props.navigation.navigate("RecipesByCategoryScreen", {
      IdCategory: category_id,
      TitleCategory: category_title,
    });
  };

  _renderItem({ item, index }) {
    return (
      <TouchableOpacity
        onPress={this.RecipesByCategory.bind(
          this,
          item.category_id,
          item.category_title
        )}
        activeOpacity={1}
        style={{ flex: 1, marginRight: 10 }}
      >
        <ImageBackground
          source={{
            uri: "http://wecheck.co.za/admin/images/" + item.category_image,
          }}
          style={{
            height: 100,
            width: width * 0.7,
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            overflow: "hidden",
          }}
          imageStyle={{
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            overflow: "hidden",
          }}
        ></ImageBackground>
        <View
          style={{
            padding: 6,
            width: width * 0.7,
            borderColor: "#EEE",
            borderTopWidth: 0,
            borderWidth: 1,
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
          }}
        >
          <Text
            numberOfLines={1}
            style={{
              color: "#000",
              fontSize: 14,
              fontFamily: "Notosans-normal",
              marginBottom: 5,
            }}
          >
            {item.category_title.toUpperCase()}
          </Text>
          <View style={{ flexDirection: "column", marginBottom: 5 }}>
            <Text style={{ fontSize: 13, color: "#a4a4a4", marginBottom: 5 }}>
              {item.category_title}
            </Text>
            <ItemRating itemId={item.recipe_id} starSize={15} starWidth={75} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    if (this.state.isLoading) {
      return <AppPreLoader />;
    }

    const { params } = this.props.navigation.state;

    return (
      <Carousel
        ref={(c) => {
          this._carousel = c;
        }}
        data={this.state.categories}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={this.RecipesByCategory.bind(
              this,
              item.category_id,
              item.category_title
            )}
            activeOpacity={1}
            style={{ flex: 1, marginRight: 10 }}
          >
            <ImageBackground
              source={{
                uri: "http://wecheck.co.za/admin/images/" + item.category_image,
              }}
              style={{
                height: 100,
                width: width * 0.7,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                overflow: "hidden",
              }}
              imageStyle={{
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                overflow: "hidden",
              }}
            ></ImageBackground>
            <View
              style={{
                padding: 6,
                width: width * 0.7,
                borderColor: "#EEE",
                borderTopWidth: 0,
                borderWidth: 1,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
              }}
            >
              <Text
                numberOfLines={1}
                style={{
                  color: "#000",
                  fontSize: 14,
                  fontFamily: "Notosans-normal",
                  marginBottom: 5,
                }}
              >
                {item.recipe_title}
              </Text>
              <View style={{ flexDirection: "column", marginBottom: 5 }}>
                <Text
                  style={{ fontSize: 13, color: "#a4a4a4", marginBottom: 5 }}
                >
                  {item.category_title}
                </Text>
                <ItemRating
                  itemId={item.recipe_id}
                  starSize={15}
                  starWidth={75}
                />
              </View>
            </View>
          </TouchableOpacity>
        )}
        sliderWidth={width}
        itemWidth={width * 0.7}
        layout={"default"}
        firstItem={1}
        loop={true}
        activeSlideOffset={2}
      />
    );
  }
}

export default withNavigation(CategoriesHome);
